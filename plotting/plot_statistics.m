%PLOT_STATISTICS   Plots results of one or more LHS experiments.
%   Results are a combination of an output statistic (eg, Attack Rate) and a
%   transformation function (eg, mean). The transformation function is applied
%   to all of the LHS samples for each combination of control parameter values.
%   A plot is produced for each result, comparing each of the LHS experiments. 
%
%   figures = PLOT_STATISTICS(statistics, functions, experiments);
%
%   STATISTICS are specified as a single integer or vector of integers, which
%   specify the statistic(s) BY INDEX; or as a single string or cell array of
%   strings, which specify the statistic(s) BY NAME.
%
%   FUNCTIONS is either a SINGLE FUNCTION HANDLE, used for deriving each of the
%   results, or a CELL ARRAY OF FUNCTION HANDLES, one for each of the specified
%   statistics.
%
%   EXPERIMENTS are the lhs_data structures of the experimental results (eg, as
%   returned by MOST_RECENT) and are passed as INDIVIDUAL PARAMETERS to the
%   function.
%
%   FIGURES is an array of handles to each of the produced figures.
%
%   Example:
%      >> exp1 = most_recent('some_experiment');
%      >> exp2 = most_recent('another_experiment');
%      >> plot_statistics('Attack Rate', @mean, exp1.lhs_data, exp2.lhs_data);
%
%See also lhs, most_recent

% Copyright 2010 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function figures = plot_statistics(stats, fns, varargin)
    if isempty(varargin)
        error('plot_statistics:experiments', 'No experiments given');
    end

    exp_count = length(varargin);
    first_exp = varargin{1};
    stat_names = first_exp.stat_names;
    % the number of statistics is the size of
    % the final dimension of the stats matrix
    stats_dim = size(first_exp.stats);
    stats_available = stats_dim(end);

    if ischar(stats)
        stats = {stats};
    end
    if iscellstr(stats)
        stat_truename = zeros(1, length(stats));
        for i = 1:length(stats)
            [~, stat_nums(i)] = ismember(stats(i), stat_names);
        end
        if any(stat_nums < 1)
            error('plot_statistics:stats', 'Invalid statistics name');
        else
            stat_count = length(stat_nums);
        end
        
    elseif isnumeric(stats)
        stat_nums = stats;
        if any(stat_nums > stats_available)
           error('plot_statistics:stats', 'Invalid statistics index');
        else
            stat_count = length(stat_nums);
        end
    else
        error('plot_statistics:stats', 'Invalid statistics parameter');
    end
    
    if length(fns) == 1
        % one function for all statistics
        if iscell(fns)
            fns = repmat(fns, 1, stat_count);
        else
            fns = repmat({fns}, 1, stat_count);
        end
    elseif length(fns) ~= stat_count
        % there isn't a different function for each statistic
        error('plot_statistics:fns', 'Invalid number of functions');
    end

    figures = zeros(1, stat_count);
    for i = 1:stat_count
        if ~isfield(first_exp,'stat_layout')
            stat_entries = stat_nums(i);
            stat_name = first_exp.stat_names{stat_nums(i)};
        else % stats are a structure so we stat_nums(i) may refer to multiple entries in stats_dim
            seq_of_entries = structfun(@(x) x(1), first_exp.stat_layout);
            cum_seq_of_entries = [0 ; cumsum(seq_of_entries(1:end-1))];
            stat_entries = (1:seq_of_entries(stat_nums(i))) + cum_seq_of_entries(stat_nums(i));
            stat_name = stats{i};
        end
            figures(i) = plot_stat(stat_name, stat_entries, fns{i}, varargin{:});
    end

    fprintf(1, 'Experiments:\n');
    for i = 1:length(varargin)
        fprintf(1, '%s (%s)\n', varargin{i}.exp_name, varargin{i}.mat_file);
    end
    fprintf(1, '\n');
    fprintf(1, 'Statistics:\n');
    for i = 1:stat_count
        if ~isfield(first_exp,'stat_layout')
            fprintf(1, '%s\n', first_exp.stat_names{stat_nums(i)});
        else
            fprintf(1, '%s\n', stats{i});
        end
    end
end

%PLOT_STAT   Plots a single result for one or more LHS experiments.
%   Plots a result over a given set of LHS experiments. The result is specified
%   as a function of a single statistic gathered over all of the LHS samples
%   for a single set of control parameter values.
%
%   figure = PLOT_STAT(statistic, function, experiments);
function h = plot_stat(stat_name, stat, fn, varargin)
    h = figure();
    hold('off');

    for i = 1:length(varargin)
        exp = varargin{i};
        if i == 2
            % hold after plotting the result of the first experiment
            hold('on');
        end

        cpar_count = length(exp.control_params);
        if cpar_count == 1
            stat_data = exp.stats(:, :, stat);
            cpar_vals = exp.control_params{1}.values;
            cpar_count = length(cpar_vals);
            plot_data = zeros(1, cpar_count);
            for c = 1:cpar_count
                plot_data(c) = fn(stat_data(c, :, :));
            end
            plot(cpar_vals, plot_data);
            if i == 1
                % Set the axis labels appropriately
                xlabel(cpar_name(exp.control_params{1}));
                ylabel(stat_name);
            end
        elseif cpar_count == 2
            stat_data = exp.stats(:, :, :, stat);
            cpar1_vals = exp.control_params{1}.values;
            cpar2_vals = exp.control_params{2}.values;
            cpar1_len = length(cpar1_vals);
            cpar2_len = length(cpar2_vals);
            plot_data = zeros(cpar1_len, cpar2_len);
            combs = combvec(1:cpar1_len, 1:cpar2_len);
            for c = 1:size(combs, 2)
                x = combs(1, c);
                y = combs(2, c);
                plot_data(x, y) = fn(stat_data(x, y, :, :));
            end
            [xs, ys] = meshgrid(cpar1_vals, cpar2_vals);
            mesh(xs', ys', plot_data);
            if i == 1
                % Set the axis labels appropriately
                xlabel(cpar_name(exp.control_params{1}));
                ylabel(cpar_name(exp.control_params{2}));
                zlabel(stat_name);
            end
        else
            error('plot_statistics:cpars', ...
                'Invalid number of control parameters: %d', cpar_count);
        end
    end
    hold('off');
end

%CPAR_NAME   Returns the description of a control parameter
%   If no description is provided, CPAR_NAME returns the parameter name.
function name = cpar_name(control_param)
    if isfield(control_param, 'description')
        name = control_param.description;
    else
        name = control_param.name;
    end
end
