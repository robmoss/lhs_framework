%PLOT_STATPARAMS_HEAT   Plots results of one or more LHS experiments.
% plot_statparams_heat(param1,p1_box,p1_fn,param2,p2_box,p2_fn,stats,stats_to_vec_fn,statfn, varargin)
% param1,param2 == parameters
% p1_box, p2_box == number of boxes to divide axes into
% p1_fn,p2_fn == function to ensure param1/2 is a scalar. Use @(x) x if
% already scalar
% p1axis,p2axis either 'lin' or 'log' for linear or log scaling of the parameter axis
% stats == output statistics to be plotted
% stat_to_vec_fn == fn to collapse the statistic into a vector, with an
% entry for each value of (param1,param2). Use @(x) x if already a vector
% stat_fn == could be mean, std, AR_lt_10, the function to apply to the set
% of values from stat_to_vec_fn(stats) to produce a scalar for the z-axis
% surface plot
% c_axis == color axis. For auto color pass -1. Useful for fixing a
% consistent color map over multiple figures, must be a 1x2 vector, e.g. [0 1].
% varargin == expt.lhs_data
%
% Can specify stats or params by either name or number
% Based very heavily on Rob's plot_statistics.m
%
%   Results are a combination of an output statistic (eg, Attack Rate) and a
%   parameter. This command only works with 1D LHS experiments and is generally
%   most suited to those 1D LHS experiments performed with a dummy parameter.
%   Consistency checking is performed to ensure both the statistic and
%   the parameter passed are valid values.
%
%   Borrows heavily from Rob's plot_statistics.m
%
%   figures = PLOT_STATPARAMS(statistic, parameter, experiments);
%
%   STATISTICS are specified as a single integer, which
%   specify the statistic BY INDEX; or as a single string 
%   which specifies the statistic BY NAME.
%
%   PARAMETERS are specified as a single string which specifies the parameter by
%   name.
%
%   EXPERIMENTS are the lhs_data structures of the experimental results (eg, as
%   returned by MOST_RECENT) and are passed as INDIVIDUAL PARAMETERS to the
%   function.
%
%   FIGURES is an array of handles to each of the produced figures.
%
%   Example:
%      >> exp1 = most_recent('some_experiment');
%      >> exp2 = most_recent('another_experiment');
%      >> plot_statistics('Attack Rate', 'R0', exp1.lhs_data, exp2.lhs_data);
%
%See also lhs, most_recent

% Copyright 2011-13 Mathew Dafilis, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function figures = plot_statparams_heat(param1,p1_box,p1_fn,p1axis,param2,p2_box,p2_fn,p2axis,stats,stat_to_vec_fn,stat_fn,c_axis,varargin)
% Test to see if any experiments in varargin
    if isempty(varargin) % If none bye-bye
        error('plot_statparams:experiments', 'No experiments given');
    end

    % Statistics manipulation
    
     exp_count = length(varargin);
     first_exp = varargin{1};
     stat_names = first_exp.stat_names;
     % the number of statistics is the size of
     % the final dimension of the stats matrix
     stats_dim = size(first_exp.stats);
     stats_available = stats_dim(end);
% for i = 1:length(stats)
     
     if ischar(stats)
         stats = {stats};
     end
     if iscellstr(stats)
         [~, stat_nums] = ismember(stats, stat_names);
         if any(stat_nums < 1)
             error('plot_statistics:stats', 'Invalid statistics name');
         end
     elseif isnumeric(stats)
         stat_nums = stats;
         if any(stat_nums > stats_available)
            error('plot_statistics:stats', 'Invalid statistics index');
         end
     else
         error('plot_statistics:stats', 'Invalid statistics parameter');
     end
    
     % Parametric manipulation
     
     param_names=fieldnames(first_exp.parameter_samples);
     param_dim=size(param_names);
     param_available=param_dim(end);
     
     if ischar(param1)
         param1={param1};
     end
     
     if iscellstr(param1)
         [~,param_nums] = ismember(param1,param_names);
     end
     if any(param_nums < 1)
             error('plot_paramstats3D:param1', 'Invalid parameter index');
     end
 
 if ischar(param2)
         params={param2};
     end
     
     if iscellstr(param2)
         [~,param_nums] = ismember(param2,param_names);
     end
     if any(param_nums < 1)[0 1]
             error('plot_paramstats3D:param1', 'Invalid parameter index');
     end
     
     first_exp=varargin{1};
     if ~isfield(first_exp,'stat_layout')
         stat_entries = stat_nums;
         stat_name = first_exp.stat_names{stat_nums};
     else % stats are a structure so stat_nums(i) may refer to multiple entries in stats_dim
         layout = rmfield(first_exp.stat_layout,'size');
         seq_of_entries = structfun(@(x) x(1), layout);
         cum_seq_of_entries = [0 ; cumsum(seq_of_entries(1:end))];
         stat_entries = (1:seq_of_entries(stat_nums)) + cum_seq_of_entries(stat_nums);
         stat_name = stats;
     end

	% Create a new figure
    figures = figure();
    hold on; % Keep plots
    for i = 1:length(varargin);   % Loop over number of experiments
        exp=varargin{i};	% Pick out an experiment
        stat_data = stat_to_vec_fn(exp.stats(:, :, stat_entries)); % Get the stat data
        param1_data=p1_fn(getfield(exp.parameter_samples, char(param1))); % Get the param data
        param2_data=p2_fn(getfield(exp.parameter_samples, char(param2)));
        [xd yd zd] = mpdmegamean_bb(param1_data,param2_data,stat_data,stat_fn, p1_box,p1axis,p2_box,p2axis);
        
        surf(xd,yd,zd); % Plot them up
        if ~(c_axis == -1)
            caxis(c_axis);
        end
        shading interp
        xlabel(param1)
        ylabel(param2) % Label the graph
        zlabel(exp.stat_names(stat_nums))
        colorbar()
        %view(-45,45)
        if char(p1axis) == 'log'
            set(gca,'XScale','log')
        end
        if char(p2axis) == 'log'
            set(gca,'YScale','log')
        end
    end
        
% Mega mean
% MPD Wed 8 Dec 2010
%
function [xd yd zd]= mpdmegamean_bb(p1,p2,st,fn,bs1,faxis1,bs2,faxis2)
dat=sortrows([p1',p2',st']);
%disp(['Data size ',num2str(size(dat))]);
%
dat(any(isnan(dat),2),:)=[];
%disp(['Data size post NaN removal ',num2str(size(dat))]);
%
param1=dat(:,1);
param2=dat(:,2);
stat=dat(:,3);
%
maxp1=max(param1);
minp1=min(param1);
maxp2=max(param2);
minp2=min(param2);
md1=max(param1)-min(param1);
md2=max(param2)-min(param2);
%
% Cut points
%cp1=zeros(1,bs1);
%cp2=zeros(1,bs2);
if char(faxis1) =='lin'
    cp1=linspace(minp1,maxp1,bs1);
elseif char(faxis1) == 'log'
    cp1=logspace(log10(minp1),log10(maxp1),bs1);
else
    error('Function 1 illdefined');
end
if char(faxis2) == 'lin'
    cp2=linspace(minp2,maxp2,bs2);
elseif char(faxis2)=='log'
    cp2=logspace(log10(minp2),log10(maxp2),bs2);
else
    error('Function 2 illdefined');
end
%
% First bin is the min of the data set
%cp1(1)=minp1;
%cp2(1)=minp2;
% Remaining bins linearly distributed
%for ii=[2:bs1]
%    cp1(ii)=(md1/(bs1-1))*(ii-bs1)+maxp1;
%end
%for ii=[2:bs2]
%    cp2(ii)=(md2/(bs2-1))*(ii-bs2)+maxp2;
%end
% Last bin is the max of the data set
%cp1(end)=maxp1;
%cp2(end)=maxp2;
%
xd=zeros(bs1-1,bs2-1);
yd=zeros(bs1-1,bs2-1);
zd=zeros(bs1-1,bs2-1);
for ii=[1:bs1-1]
    for jj=[1:bs2-1]
        xind = (param1 > cp1(ii)) & (param1 < cp1(ii+1));
        yind = (param2 > cp2(jj)) & (param2 < cp2(jj+1));
        %
        xd(ii,jj)=mean( param1( xind ) );
        yd(ii,jj)=mean( param2( yind ) );
        zd(ii,jj)=fn( stat(xind & yind ));
    end
end
