%PARTITION_PARAMETERS   Return the parameter values in a single LHS partition.
%   Given a single partition of an LHS experiment, the values of the model
%   parameters in this partition are returned in a matrix. The matrix can be
%   restricted to a subset of the model parameters.
%
%   param_vals = partition_parameters(lhs_data, partition, params);
%
%   LHS_DATA is the LHS experiment from which the partition is taken.
%
%   PARTITION identifies a partition of the LHS simulations.
%
%   PARAMS is an optional cell array of parameter names, defining the subset of
%   model parameters to be returned; the default is to return all parameters.
%
%   PARAM_VALS is an NxP matrix for N simulations in the partition and P model
%   parameters.
%
%   Example:
%      >> parts = partition_randomly(lhs_data, 5);
%      %  Return the parameter values for the fifth random partition
%      >> param_vals = partition_parameters(lhs_data, parts, 5);
%
%See also partition_by_params, partition_randomly, partition_statistic

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function param_vals = partition_parameters(lhs_data, partition, params)
    if nargin < 3
        params = fieldnames(lhs_data.parameter_samples);
    end

    entries = nnz(partition);
    param_count = length(params);
    param_vals = zeros(entries, param_count);
    for i = 1:param_count
        values = lhs_data.parameter_samples.(params{i});
        param_vals(:, i) = values(partition);
    end
end
