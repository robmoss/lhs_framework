%PARTITION_RANDOMLY   Partitions a collection of simulations at random.
%   This function randomly divides the simulations from a single LHS experiment
%   into groups for the purpose of fitting and evaluating a regression model.
%
%   points = PARTITION_RANDOMLY(lhs_data, partitions);
%
%   LHS_DATA is the LHS experiment to partition.
%
%   PARTITIONS is the number of random groups to return.
%
%   POINTS is an array where each entry identifies the partition to which the
%   corresponding simulation belongs.
%
%   Example:
%      % Partition the results into 5 random groups
%      >> points = partition_randomly(lhs_data, 5);

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function points = partition_randomly(lhs_data, partitions)
    param_names = fieldnames(lhs_data.parameter_samples);
    space_dims = size(lhs_data.parameter_samples.(param_names{1}));
    points = 1 + floor(rand(space_dims) * partitions);
end
