This directory contains fixed-step ODE solvers that are available for download
from the MathWorks website but are not officially supported, as per Technical
Solution 1-1TJ3GZ:

The ability to use a fixed-step solver is not built into MATLAB. 

Fixed-step solvers are available for download at the following link as part of
a technical note about solving ODEs in MATLAB:
http://www.mathworks.com/support/tech-notes/1500/1510.html#fixed

Because they do not ship with MATLAB, these solvers are not officially
supported.

