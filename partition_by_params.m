%PARTITION_BY_PARAMS   Partition an LHS experiment by parameter values.
%   For a given set of parameter-space samples (ie, the result of some LHS
%   experiment) divide the simulations into partitions, where partitions are
%   defined as equal subcollections over some parameter.
%
%   points = PARTITION_BY_PARAMS(lhs_data, partitions)
%
%   LHS_DATA is the LHS experiment to partition.
%
%   PARTITIONS is a record (structure array) where each fieldname identifies a
%   parameter to partition, and the value of the field specifies how many
%   partitions for that parameter.
%
%   POINTS is the matrix of partition values.
%
%   Example:
%      % Partition the results into 30 (6x5) partitions
%      >> partitions = struct('alpha', 6, 'beta', 5);
%      >> points = partition_by_params(lhs_data, partitions);

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function points = partition_by_params(lhs_data, partitions)
    param_names = fieldnames(partitions);
    space_dims = size(lhs_data.parameter_samples.(param_names{1}));
    points = ones(space_dims);
    points = partition_parameters(points, lhs_data, param_names, ...
        partitions, 1, 1);
end

%PARTITION_PARAMETER   Partitions a subspace of simulations by parameters.
%   The simulations are recursively divided into partitions of equal size.
%
%   points = partition_parameters(points, lhs_data, param_names, param_parts,
%   param_index, parent_num);
%
%   POINTS is the N-dimensional cell array that stores the partitions over N
%   parameters.
%
%   LHS_DATA is the LHS experiment containing the simulations that are to be
%   partitioned.
%
%   PARAM_NAMES is the cell array of parameter names by which the simulations
%   are to be partitioned.
%
%   PARAM_PARTS is the record (structure array) that defines how many
%   partitions should be created for each parameter.
%
%   PARAM_INDEX is the index (into PARAM_NAMES and PARAM_PARTS) that identifies
%   the parameter that defines the next set of partitions.
%
%   PARENT_NUM is the number that uniquely identifies the parent partition
%   (ie, the simulations that are to be partitioned).
%
%   Example:
%      >> partitions = struct('alpha', 6, 'beta', 5);
%      % Sub-partition the third partition by values of beta
%      >> points = partition_parameters(points, lhs_data, {'alpha' 'beta'},
%            partitions, 2, 3);
function points = partition_parameters(points, lhs_data, param_names, param_parts, param_index, parent_num)
    % calculate the numbering gap between the partitions for this parameter
    skip_amount = skip(param_parts, param_names, param_index);
    % determine the set of simulations under consideration
    parent_part = (points == parent_num);
    % ensure that this partition is not empty
    if nnz(parent_part) == 0
        error('partition_by_params:parent', ...
            'Empty parent partition for parameter #%d', param_index);
    end
    % determine the parameter to partition by
    partition_param = param_names{param_index};
    % determine the number of partitions to create;
    partition_count = param_parts.(partition_param);
    % retrieve ALL values for this parameter
    all_parameter_values = lhs_data.parameter_samples.(partition_param);
    % restrict the parameter values to the simulations under consideration
    parameter_values = all_parameter_values(parent_part);
    % sort the parameter values in order to define the partitions
    parameter_values = sort(parameter_values(:));
    % determine how big each partition should be
    partition_length = length(parameter_values) / partition_count;
    % determine the starting indices for each partition
    partition_indices = floor([0 partition_length .* (1:partition_count - 1)] + 1);
    % determine the minimum value for each partition
    partition_mins = parameter_values(partition_indices)';
    % ensure that none of the partitions are empty
    unique_mins = unique(partition_mins);
    if length(unique_mins) < length(partition_mins)
        error('partition_by_params:partition', ...
            'Empty partition for parameter #%d', param_index);
    end
    % partition ALL of the simulations
    sub_partitions = all_parameter_values >= partition_mins(1);
    for v = partition_mins(2:end)
        sub_partitions = sub_partitions + (all_parameter_values >= v);
    end
    % scale sub_partitions so that 1 -> 1, 2 -> 1 + skip_amount
    sub_partitions = (sub_partitions - 1) * skip_amount;
    % now exclude all simulations not under consideration
    sub_partitions(~ parent_part) = 0;
    % record the newly-defined partitions
    points = points + sub_partitions;

    % check if there are more partitions to define
    if param_index < length(param_names)
        new_param_index = param_index + 1;
        for p = 1:partition_count
            % divide each partition according to the next partition parameter
            new_parent_num = parent_num + (p - 1) * skip_amount;
            points = partition_parameters(points, lhs_data, param_names, ...
                param_parts, new_param_index, new_parent_num);
        end
    end
end

%SKIP   Calculate the increment in partition number for a given parameter.
%   SKIP is used to determine the sequence of partition numbers for each
%   parameter, based on the number of sub-partitions into which the partitions
%   will subsequently be divided.
%
%   amount = SKIP(partitions, param_names, param_index);
%
%   PARTITIONS is the record (structure array) that defines how many
%   partitions should be created for each parameter.
%
%   PARAM_NAMES is the cell array of parameter names by which the simulations
%   are to be partitioned.
%
%   PARAM_INDEX is the index (into PARAM_NAMES and PARAM_PARTS) that identifies
%   the parameter that defines the next set of partitions.
%
%   Example:
%      >> partitions = struct('alpha', 6, 'beta', 5, 'gamma', 4);
%      % Skip by 20 (5x4) when partitioning by alpha
%      >> amount = skip(partitions, {'alpha' 'beta'}, 1);
function amount = skip(partitions, param_names, param_index)
    param_count = length(param_names);
    amount = 1;
    for p = (param_index + 1):param_count
        amount = amount * partitions.(param_names{p});
    end
end