%LHS_RESOLUTION   Defines the resolution of the Latin Hypercube Sampling.
%   For LHS experiments involving one or two control parameters, LHS_RESOLUTION
%   provides multiple predefined sampling resolutions.
%
%   [steps samples] = LHS_RESOLUTION(control_vars, detail);
%
%   CONTROL_VARS is the number of control variables in the LHS experiment. This
%   must be either 1 or 2.
%
%   DETAIL is an optional string argument, which must be one of the following
%   values: 'low', 'medium' or 'high' (the default is 'high'). This specifies
%   how fine-grained the Latin Hypercube Sampling should be.
%
%   STEPS is the number of values that each control parameter should take
%   between their minimum and maximum values (inclusively).
%
%   SAMPLES is the number of samples to take from the model parameter space for
%   each combination of control parameter values.
%
%   Example:
%      >> [steps samples] = lhs_resolution(2, 'medium');
%
%See also lhs, example_LHS_experiment

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [steps samples] = lhs_resolution(control_vars, detail)
    % default to high resolution if no level of detail is specified
    if nargin < 2
        detail = 'high';
    end

    % ensure that the level of detail is valid
    choices = {'low' 'medium' 'high'};
    if ~ ismember(detail, choices)
        error('lhs_resolution:lod', 'invalid level of detail: %s', detail);
    end

    if control_vars == 1
        if strcmp(detail, choices{1})
            % low detail
            steps = 20;
            samples = 500;
        elseif strcmp(detail, choices{2})
            % medium detail
            steps = 50;
            samples = 1000;
        else
            % high detail
            steps = 100;
            samples = 2000;
        end
    elseif control_vars == 2
        if strcmp(detail, choices{1})
            % low detail
            steps = 6;
            samples = 500;
        elseif strcmp(detail, choices{2})
            % medium detail
            steps = 11;
            samples = 500;
        else
            % high detail
            steps = 21;
            samples = 1000;
        end
    else
        error('lhs_resolution:controls', ...
            'invalid number of control parameters: %d', control_vars);
    end
end
