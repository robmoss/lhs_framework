%% Compare histograms from multiple 1-d hypercube experiments
%

% Copyright 2010, 2012 James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [h_bars h_lines h_ksdensity] = plot_hypercube_hists(result_index, cpar_indicies, varargin)
    optargin = size(varargin, 2);
    results = cell(1, optargin);
    
    % Collate the results 
    for i = 1:optargin
        results{i} = varargin{i}.stats(cpar_indicies,:,result_index);
    end
    
    vals = zeros(optargin, numel(varargin{i}.stats(cpar_indicies,:,1)));

    % Collect the data
    for i = 1:optargin
        vals(i,:) = results{i}(:);
    end

    min_val = min(min(min(vals)));
    max_val = max(max(max(vals)));
    min_val = max(min_val, 0);
    bin_width = 0.01; % 0.02, 0.005
    bin_count = ceil((max_val - min_val) / bin_width);
    bins = linspace(min_val, max_val, bin_count);

    hist_data = zeros(optargin, bin_count);
    scale_factor = numel(varargin{i}.stats(:,:,1));
    for i = 1:optargin
        hist_data(i,:) = histc(vals(i,:), bins) / scale_factor;
    end

    colours = {{'r'}, {'g'}, {'b'}, {'c'}, {'m'}, {'y'}};

    % Plot the results from each data set in turn
    h_bars = figure();
    bars = bar(bins, hist_data');
    for i = 1:length(bars)
        set(bars(i), 'FaceColor', colours{i}{1});
        set(bars(i), 'EdgeColor', colours{i}{1});
    end
    % Set the axis labels appropriately
    xlabel(varargin{1}.stat_names{result_index});
    ylabel('Frequency');

    h_lines = figure();
    hold on
    for i = 1:optargin
        plot(bins, hist_data(i,:), colours{i}{1}, 'LineWidth', 2);
    end
    hold off
    
    h_ksdensity = figure();
    hold on
    for i = 1:optargin
        [f,xi] = ksdensity(vals(i,:));
        plot(xi,f, colours{i}{1}, 'LineWidth', 2);
    end
    hold off
    
    h_kde = figure();
    hold on
    for i = 1:optargin
        [~,f,xi] = kde(vals(i,:),2^14,-0.003,0.6);
        plot(xi,f, colours{i}{1}, 'LineWidth', 2);
    end
    hold off

    % Set the axis labels appropriately
    xlabel(varargin{1}.stat_names{result_index});
    ylabel('Frequency');
end
