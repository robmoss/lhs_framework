%PARTITION_STATISTIC   Return the output statistic in a single LHS partition.
%   Given a single partition of an LHS experiment, the values of an output
%   statistic are returned in a vector.
%
%   stat_vals = partition_statistic(lhs_data, partition, stat);
%
%   LHS_DATA is the LHS experiment from which the partition is taken.
%
%   PARTITION identifies a partition of the LHS simulations.
%
%   STAT is the name or index of an output statistic for the LHS experiment.
%
%   STAT_VALS is a Nx1 vector of the output statistic for N simulations in the
%   partition.
%
%   Example:
%      >> parts = partition_randomly(lhs_data, 5);
%      %  Return the first output statistic for the fifth random partition
%      >> stat_vals = partition_statistic(lhs_data, parts, 5, 1);
%
%See also partition_by_params, partition_randomly, partition_parameters

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function stat_vals = partition_statistic(lhs_data, partition, stat)
    stat_count = length(lhs_data.stat_names);
    param_names = fieldnames(lhs_data.parameter_samples);
    lhs_dims = size(lhs_data.parameter_samples.(param_names{1}));
    if ischar(stat)
        [~, index] = ismember(stat, lhs_data.stat_names);
        if index < 1
            error('partition_statistic:stat', 'Invalid statistic "%s"', stat);
        end
    elseif isscalar(stat) && isnumeric(stat)
        if stat > 0 && stat <= stat_count
            index = stat;
        else
            error('partition_statistic:stat', 'Invalid statistic "%f"', stat);
        end
    else
        error('partition_statistic:stat', 'Invalid statistic parameter');
    end

    range = @(len) 1:len;
    stat_inds = arrayfun(range, lhs_dims, 'UniformOutput', 0);
    stat_mat = lhs_data.stats(stat_inds{:}, index);
    stat_vals = stat_mat(partition);
end
