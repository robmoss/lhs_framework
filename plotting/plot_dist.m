% plot_dist.m
%
% Plots a parameter distribution.
%
% modified Oct 2012 by Trish Campbell to plot parameters with a 'flatint' distribution 

% Copyright 2010, 2012 James McCaw, Mathew Dafilis, Patricia Campbell.
% SPDX-License-Identifier: GPL-3.0-or-later
function plot_dist(dist)
    info = dist();

    if strcmp(info.name, 'constant') % Constant distribution
        const_val = dist(0.5);
        xs = [0.900 0.999 1.000 1.001 1.100] * const_val;
        ys = [0 0 1 0 0];
        plot(xs, ys);
    elseif strcmp(info.name, 'flat')  % Flat distribution
        minv = info.minimum;
        maxv = info.maximum;
        xs = minv:((maxv-minv)/100):maxv;
        width = maxv - minv;
        height = (1 / width) * ones(1, length(xs));
        plot(xs, height);
        axis([minv maxv 0 ceil(1 / width + 0.001)]);
    elseif strcmp(info.name, 'normal') % Normal distribution
        mean = info.mean;
        stddev = info.stddev;
        xs = linspace(mean - 3*stddev, mean + 3*stddev, 100);
        plot(xs, normpdf(xs, mean, stddev));
    elseif strcmp(info.name, 'beta') % Beta distribution
        minv = info.minimum;
        maxv = info.maximum;
        mean = info.mean;
        varn = info.variance;
        % scale the mean and variance to the [0,1] Beta distribution
        range = maxv - minv;
        scaled_mean = (mean - minv) / range;
        scaled_var = varn / range;
        % calculate the distribution parameters alpha_1 and alpha_2
        alpha_1 = (scaled_mean/scaled_var)^2 * (1 - scaled_mean) - scaled_mean;
        alpha_2 = alpha_1 * (1 - scaled_mean) / scaled_mean;
        % plot the probability density function
        xs = linspace(0, 1, 100);
        scaled_xs = minv + range * xs;
        plot(scaled_xs, betapdf(xs, alpha_1, alpha_2));
    elseif strcmp(info.name,'logflat')
        xmin=info.minimum;
        xmax=info.maximum;
        xv = [xmin xmax];
        yv = [1 1];
        plot(xv,yv);
        set(gca,'XScale','log');
    elseif strcmp(info.name,'flatint') % Flat integer distribution
        minv = info.minimum;
        maxv = info.maximum;
        xs = minv:1:maxv;
        width = maxv - minv;
        height = (1 / width) * ones(1, length(xs));
        plot(xs, height,'bo','MarkerFaceColor','b');
        axis([minv maxv 0 ceil(1 / width + 0.001)]);
    
    else
        error('plot_dist:dist_name', 'Unknown distribution: "%s"', info.name);
    end
end
