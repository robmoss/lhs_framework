%OPEN_LOG   Open a log file for recording analysis details.
%   For some experimental analyses, particularly those that compare the results
%   of several LHS experiments, it is desirable to record certain details of
%   the analysis that are not recorded in typical outputs such as plots. For
%   example, when comparing results from multiple LHS experiments it is
%   sensible to record the names of the experiments and the timestamps of the
%   result files.
%
%   fid = OPEN_LOG(exp_name, log_name);
%
%   EXP_NAME is the name of the experiment with which the log should be
%   associated. When comparing multiple experiments, a unique experiment name
%   should be used (see the following example).
%
%   LOG_NAME is the filename to which the log will be saved.
%
%   FID is the handle to the log file, which is opened for writing in text
%   mode.
%
%   Example
%      % Create a new experiment name from the filename of the analysis code
%      >> exp_name = filename_timestamped(mfilename());
%      >> log = open_log(exp_name, 'data_sets');
%      % Record the details of each experiment being compared
%      >> for i = 1:exp_count
%      >>    fprintf(log, '%s\n', exp_names(i));
%      >> end
%      >> fclose(log);

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function fid = open_log(exp_name, log_name)
    log_file = filename({'graphs' exp_name}, log_name, '.log');
    fid = fopen(log_file, 'w');
end
