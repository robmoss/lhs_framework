%PRINT_PDF   Print one or more plots to a single PDF file.
%   Given any number of figure, PRINT_PDF saves each figure as a separate page
%   in a single PDF file. The plot is saved under a timestamped directory that
%   identifies the experimental result (ie, .MAT file) from which the plots
%   were produced.
%
%   pdf_file = PRINT_PDF(mat_file, plot_name, figures);
%
%   MAT_FILE is the name of the .MAT file from which the plot(s) were derived
%   (not including the path or extension). When combining data from multiple
%   LHS experiments, a unique name should be used (see OPEN_LOG).
%
%   PLOT_NAME is a descriptive name for the plot(s) and will be included in the
%   filename of the generated PDF. As such, it should only contain valid
%   filename characters.
%
%   FIGURES is an optional scalar or vector of figure handles, which specifies
%   the figures to export. This argument is optional and defaults to the value
%   returned by GCF().
%
%   PDF_FILE is the (absolute) filename of the generated PDF file.
%
%   Example:
%      % Export a plot of a single LHS experiment
%      >> [data mat_file] = most_recent('some_exp');
%      >> h = produce_some_plot(data.lhs_data);
%      >> print_pdf(mat_file, 'some_plot', h);
%
%      % Export a plot of multiple LHS experiments
%      >> data2 = most_recent('another_exp');
%      >> h2 = produce_another_plot(data.lhs_data, data2.lhs_data);
%      % Create a unique name for this plot
%      >> exp_name = filename_timestamped('another_plot');
%      >> print_pdf(exp_name, 'another_plot', h2);
%
%See also: open_log

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [pdf_file] = print_pdf(mat_file, plot_name, figures)
    if nargin < 3
        figures = gcf;
    end

    eps_file = filename({'graphs' mat_file}, plot_name, '.eps');
    pdf_file = filename({'graphs' mat_file}, plot_name, '.pdf');

    print(figures(1), '-depsc2', eps_file);
    for i = 2:length(figures)
        print(figures(i), '-depsc2', '-append', eps_file);
    end
    system(['epstopdf "' eps_file '"']);
    system(['rm "' eps_file '"']);
end
