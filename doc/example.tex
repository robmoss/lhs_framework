To illustrate how this framework can be applied to an arbitrary model, we
present a simple \ac{SIR} model (\path{example_SIR_model.m}) and an
experiment (\path{example_LHS_experiment.m}) that explores the model
dynamics.

\subsection{The Matlab Search Path}
In order to use the \ac{LHS} framework, the directory containing the \ac{LHS}
framework code should be added to the user's \path{MATLAB_PATH} environment
variable. For example, add the following lines to \path{.bashrc} or
\path{.profile}:

\begin{lstlisting}[language=bash]
if [ "${MATLABPATH}" = "" ]; then
    export MATLABPATH="${HOME}/lhs_framework"
else
    export MATLABPATH="${HOME}/lhs_framework:${MATLABPATH}"
fi
\end{lstlisting}

\subsection{The SIR Model}
A model consists of several components, all stored in a single \emph{record}
(structure array) and referred to by name, as demonstrated by the model
definition (\path{example_SIR_model.m}):

\begin{lstlisting}
function model = example_SIR_model()
    % collect the various parts of the model into a single structure
    model = struct( ....
        'params', @params, 'state', @state, 'statistics', @stats, ...
        'default_dists', @def_dists, 'dependent_dists', @dep_dists, ...
        'events', @events, 'equations', @rhs ...
        );
end
\end{lstlisting}

These components are now introduced in turn, beginning with the model
parameters. Model parameters are stored in a single record and referred to by
name. In addition to model equation parameters, additional parameters are
required to define certain aspects of the simulation: \path{time_start} and
\path{time_end} specify the simulation time-range; \path{time_entries}
specifies the temporal resolution required of the simulations; and the optional
parameter \path{solve_discretely} defines whether simulations should proceed
with a continuous solver (\path{solve_discretely = 0}) or a fixed time-step
(Euler's method) solver (\path{solve_discretely = 1}). The default is
\path{solve_discretely = 0} (\ie use a continuous solver).

\begin{lstlisting}
function params = params()
    params = struct( ...
        'N', 1e4, ... % a population of 10,000
        'Z', 0.1, ... % 10% are immune
        'I0', 10, ... % 10 initial cases in the population
        'R0', 1.5, ... % an R0 of 1.5
        'gamma', 365 / 1, ... % an infectious phase of 1 day
        'beta', 547.5, ... % beta = R0 * gamma
        'solve_discretely', 1, ... % always solve discretely
        'time_start', 0, ... % start simulations at time t=0
        'time_end', 42 / 365, ... % end simulations at 6 weeks
        'time_entries', 42 ... % record the model state once per day
        );
end
\end{lstlisting}

The initial state of the model is derived from a given set of parameter values:

\begin{lstlisting}
function state = state(params)
    R = params.Z * params.N;
    I = params.I0;
    S = params.N - (I + R);
    state = [S I R];
end
\end{lstlisting}

To compare model dynamics over some parameter space, it is convenient to reduce
each simulation to a set of key statistics rather than to compare the
time-series for each state. Indeed, the \ac{LHS} framework does not record
these time-series and requires some function to generate statistics for each
simulation. When no arguments are given this function should return a
descriptive name for each statistic (this is used to label plot axes).

\begin{lstlisting}
function output = stats(time, params, state, events)
    if nargin == 0
        % Return the names of the statistics being recorded
        output = {'Attack Rate', 'AR 50%', 'Total Infections'};
        return;
    end
    
    % Extract the time-series for each state variable
    S = state(:,1);
    I = state(:,2);
    R = state(:,3);
    
    % Calculate the desired statistics
    init_R = params.Z * params.N;
    total_infections = R(end) - init_R;
    attack_rate = total_infections / params.N;
    attack_rate_50 = 365 * ...
        time(find( (R - init_R) / params.N >= 0.5 * attack_rate, 1 ));

    % Record all of the statistics
    output = [ attack_rate attack_rate_50 total_infections ];
end
\end{lstlisting}

When using \ac{LHS}, parameter values are chosen at random from well-defined
probability distributions, which must be provided by the model. The \ac{LHS}
understands two sets of distributions (defined separately): those for
independent parameters, and those for parameters whose distributions or values
are derived from other parameters. The probability distributions for
independent parameters can optionally be defined for a number of distinct
scenarios:

\begin{lstlisting}
function dists = def_dists(scenario)
    % distributions are only needed for parameters that should be varied
    % parameters that should only take their default value can be ignored
    dists = struct( ...
        'R0', dist_beta(1.5, 0.1, 1.1, 1.9), ... % R0 varies from 1.1 to 1.9
        'Z', dist_flat(0.05, 0.15) ... % 5% to 15% are initially immune
        );

    if nargin > 0
        if strcmp(scenario, 'severe')
            % make R0 higher, to produce a more severe epidemic
            dists.R0 = dist_beta(1.8, 0.1, 1.4, 2.2);
        else
            % warn that an unrecognised scenario was specified
            warning('example_SIR_model:scenario', ...
                'Unrecognised scenario "%s"', scenario);
        end
    end
end
\end{lstlisting}

The probability distributions for dependent parameters are derived solely from
the values of the independent parameters):

\begin{lstlisting}
function dists = dep_dists(params)
    if nargin == 0
        % return the names of the dependent parameters
        dists = {'I0', 'beta'};
        return;
    end

    % initial cases are (on average) 0.9% of the susceptible population
    % therefore, the number of cases depends on both N and Z
    I0_mean = params.N * (1 - params.Z) * 0.009;
    I0_var = 0.15 * I0_mean;
    I0_min = 0.5 * I0_mean;
    I0_max = 1.5 * I0_mean;

    % return the probability distributions
    dists = struct( ...
        'I0', dist_beta(I0_mean, I0_var, I0_min, I0_max), ...
        'beta', dist_const(params.R0 * params.gamma) ...
        );
end
\end{lstlisting}

There may also be events of interest during a simulation, the details of which
can be stored in a global record (\path{events}). Typical uses for this
facility are to determine whether a specific condition arose during a
simulation, or at what time some threshold was met. The model is responsible
for initialising this record:

\begin{lstlisting}
function events = events(initial_state, params)
    events = struct( ...
        'largeCaseLoad', 0, ... % set to 1 when I exceeds 100
        'timeDoubledR', -1 ... % set when R becomes twice as big as at t = 0
        );
end
\end{lstlisting}

The final part of the model is the system of equations that describe the model
dynamics. This is provided as a function that accepts the current time, the
current model states and the chosen parameter values, and returns the rate of
change of each model state. To implement a model that can behave
deterministically \emph{or} stochastically, use the value of
\path{solve_discretely} to determine whether to solve deterministic or
stochastic equations.

\begin{lstlisting}
function rates = rhs(time, state, params)
    global events;

    % extract the individual states from the current state vector
    S = state(1);
    I = state(2);
    R = state(3);

    % check whether any events of interest have occurred
    if I > 100
        events.largeCaseLoad = 1;
    end
    if events.timeDoubledR == -1 && R >= 2 * params.N * params.Z
        events.timeDoubledR = time;
    end

    % calculate the flows between the states
    S_to_I = params.beta * I * S / params.N;
    I_to_R = params.gamma * I;

    % use stochastic flows if a discrete solver is being used and the
    % deterministic flow is less than 10 (persons per year)
    if params.solve_discretely && S_to_I < 10
        stoch_vals = events.stochStream.rand([floor(S) 1]);
        pr = params.beta * I / params.N;
        % NOTE: if beta * I > N, the stochastic rate is clipped at S
        % this is not an issue, since stochastic flows are only used
        % when the corresponding deterministic flows are very small
        S_to_I = sum(stoch_vals < pr);
    end
    if params.solve_discretely && I_to_R < 10
        stoch_vals = events.stochStream.rand([floor(I) 1]);
        pr = params.gamma;
        I_to_R = sum(stoch_vals < pr);
    end

    % calculate the net flow rate into each state
    dS = - S_to_I;
    dI = S_to_I - I_to_R;
    dR = I_to_R;

    % return the flow rates (as a column vector, not a row vector)
    rates = [dS; dI; dR];
end
\end{lstlisting}

Several of the model components are optional: \path{events} can be omitted if
there are no events of interest, and \path{dependent_dists} can be omitted if
there are no dependent parameters. It is possible, but not recommended, to omit
\path{default_dists}.

\subsection{The LHS Experiment}
A \ac{LHS} experiment combines a predefined model with probability
distributions for any number of model parameters and defines one or more
\emph{control parameters} that are varied systematically, as illustrated by the
example experiment (\path{example_LHS_experiment.m}):

\begin{lstlisting}
function [mat_file] = example_LHS_experiment()
    % load the model
    model = example_SIR_model();

    % define the parameter distributions
    param_dists = model.default_dists('severe');
    param_dists.I0 = dist_const(2);

    % choose parameter steps and sample numbers for medium resolution
    [steps samples] = lhs_resolution(2, 'medium');

    % define the control parameters
    param1 = struct(...
        'name', 'Z', ...
        'description', 'Prior Immunity', ...
        'values', linspace(0.0, 0.5, steps));
    param2 = struct(...
        'name', 'gamma', ...
        'description', 'Infectious Phase', ...
        'values', 365 / linspace(1.0, 3.0, steps));

    % run the LHS simulations and save the results
    [mat_file] = lhs(model, param_dists, samples, param1, param2);
end
\end{lstlisting}

\subsection{Resuming Experiments}
Should an \ac{LHS} experiment terminate unexpectedly (\eg the computer crashes
or IT staff unplug an important cable), the experiment can be easily resumed
from the most recently completed point in the control parameter space.

\begin{lstlisting}[texcl]
% begin the LHS experiment
example_LHS_experiment();
% and the experiment is interrupted $\dots$
% resume by providing the contents of the snapshot file
filedata = load('/some/path/to/snapshot.mat');
mat_file = lhs_resume(filedata.lhs_snapshot);
% alternatively, resume by specifying the experiment name
mat_file = lhs_resume('example_LHS_experiment');
\end{lstlisting}

\subsection{Analysing LHS Results}
In general, the model simulation and data analysis are performed
separately. Experimental results are retrieved with \path{most_recent}, which
loads the most recent results for a given experiment. An optional argument
allows for the nth-most-recent results to be returned. Once the results are
retrieved, they are ready for analysis.

A simple first analysis step is to plot one or more output statistics over the
control parameter space. Since multiple model simulations are performed at each
point (\eg 500 simulations for \path{example_LHS_experiment}) some
transformation is required to reduce the multiple values for the output
statistic into a single number (\eg \path{mean}). The following example shows
how to plot the mean attack rate:

\begin{lstlisting}[texcl]
% run the experiment
example_LHS_experiment();
[example_results mat_file] = most_recent('example_LHS_experiment');
% plot the mean attack rate
fig = plot_statistics('Attack Rate', @mean, example_results.lhs_data);
view(135, 30);
% save the plot as a PDF (shown in \autoref{fig:mean_ar})
print_pdf(mat_file, 'mean_attack_rate', fig);
% plot how often the attack rate was less than 10\%
AR_lt_10 = @(xs) nnz(xs < 0.1) / length(xs(:));
fig = plot_statistics('Attack Rate', AR_lt_10, example_results.lhs_data);
\end{lstlisting}

\begin{figure}
  \centering
  \includegraphics[width=0.67\textwidth]{mean_attack_rate}
  \caption{The mean attack rate over the 2D space defined by the control
    parameters.}
  \label{fig:mean_ar}
\end{figure}

Note that \path{plot_statistics} is more flexible than in the example given
here: multiple output statistics, multiple experiments and multiple statistical
transformations can be plotted (see the Matlab help for \path{plot_statistics}
for further details).

\subsection{Fitting \acp{GLM} to Simulation Data}
Given the results shown in \autoref{fig:mean_ar}, it would seem reasonable for
a \acf{GLM} to predict the final attack rate given an estimate of the proportion
of the population that has prior immunity. We can fit a \ac{GLM} based on the
results of the LHS experiment and compare it to the simulation results:

\begin{lstlisting}[texcl]
[example_results mat_file] = most_recent('example_LHS_experiment');
lhs_data = example_results.lhs_data;
% fit a one-parameter GLM to the synthetic data; \path{glm_for_stat}
% chooses parameter(s) that are most highly correlated with the output
[b dev names] = glm_for_stat(lhs_data, 'Attack Rate', 1);
b =
    0.7570 % constant term
   -1.5519 % parameter coefficient
dev =
   15.8093 % deviance of the fit (a generalisation of the residual sum of squares)
names =
   'Z' % the name of the chosen parameter
% randomly separate the synthetic data into five partitions
parts = partition_randomly(lhs_data, 5);
% check whether the GLM exhibits bias in any of the partitions
dev = glm_deviance(lhs_data, parts, names, 'Attack Rate', b);
\end{lstlisting}

Deviance is calculated as the sum of the squared differences between the GLM
predictions and the synthetic data. The sum of the squared differences between
the 95\% confidence bounds for the GLM predictions and the synthetic data can
also be calculated (see the Matlab help for \path{glm_for_stat} and
\path{glm_deviance} for details).

We can also derive a \ac{GLM} to predict whether or not the final attack rate
will be less than 50\%. Since the predicted outcome is \emph{boolean} (\ie true
or false) the quality of the \ac{GLM} can be measured by a \ac{ROC} curve. In
the following example, a \ac{GLM} is derived from a random fraction of the
simulations and the fit is evaluated with an \ac{ROC} curve.

\begin{lstlisting}[texcl]
[example_results mat_file] = most_recent('example_LHS_experiment');
lhs_data = example_results.lhs_data;
% randomly separate the synthetic data into 10 partitions
rand_parts = partition_randomly(lhs_data, 10);
% derive the GLM from the 10th random partition
glm_part = rand_parts == 10;
% use the logit link function
link_fn = 'logit';
% classify the attack rate as being below or above 50\%
transform = @(x) x < 0.5;
% derive the GLM
[b dev names rho stats] = glm_for_stat(lhs_data, 'Attack Rate', 1, ...
    'distr', 'binomial', 'link', link_fn, 'fn', transform, ...
    'partition', glm_part);
% plot the ROC curve; the integral is 0.992 (shown in \autoref{fig:roc})
glm_roc(lhs_data, names, 'Attack Rate', b, 'link', link_fn, 'fn', transform);
% save the plot
print_pdf(mat_file, 'glm_roc');
\end{lstlisting}

\begin{figure}
  \centering
  \includegraphics[width=0.67\textwidth]{glm_roc}
  \caption{The \ac{ROC} curve for the single-parameter \ac{GLM} that predicts
    whether or not the final attack rate will be less than 50\%.}
  \label{fig:roc}
\end{figure}

\subsection{Comparing Simulations}
For any \ac{LHS} experiment, the \path{statistics} function can be extended to
do more than analyse the time-series data for each simulation. For example, it
is straightforward to tweak the simulation parameters, run a new simulation and
compare the time-series data of the two simulations. This can be used to
calculate the \ac{NRMSD} at \emph{every} point in the control parameter
space. The following statistics function compares the results of simulating the
example model using the continuous and discrete solvers, under the assumption
that the \ac{LHS} experiment is using the continuous solver:

\begin{lstlisting}
function output = compare_solvers(cont_time, params, cont_state, events)
    if nargin == 0
        output = {'NRMSD(S)' 'NRMSD(I)' 'NRMSD(R)'};
        return;
    end
    % Use the discrete model solver
    params.solve_discretely = 1;
    model = example_SIR_model();
    [disc_time, disc_state, ~, ~] = solve(model, params);
    % Calculate the normalised root mean square deviation of each state
    Sdev = nrmsd(1, disc_state, cont_state);
    Idev = nrmsd(2, disc_state, cont_state);
    Rdev = nrmsd(3, disc_state, cont_state);
    output = [Sdev Idev Rdev];
end

function nrmsd = nrmsd(i, s1, s2)
    % Calculate the NRMSD of the ith state in s1 and s2
    nrmsd = sqrt( sum((s1(:,i) - s2(:,i)).^2) / length(s1) );
    mx = max(max(s1(:,i), s2(:,i)));
    mn = min(min(s1(:,i), s2(:,i)));
    if mx == mn
        nrmsd = 0;
    else
        nrmsd = nrmsd / (mx - mn);
    end
end
\end{lstlisting}

\subsection{Changing Parameter Values During a Simulation}
The model equations cannot directly change parameter values during a
simulation; any change (\eg \mlb{params.x = new_value;}) is lost at the end of
the equations function. The only way to record changes in parameters is to
store information in a \emph{global variable}. The global record \mlb{events}
is provided for this purpose, as well as for allowing events and thresholds to
be recorded. An example of changing a parameter value during a simulation is
given below:

\begin{lstlisting}
function rates = rhs(time, state, params)
    global events;

    % events.x_values is a vector of values for the parameter x
    % events.x_index specifies which value to use
    params.x = events.x_values(events.x_index);

    if some_condition
        % use the next value of x from this point on
        events.x_index = events.x_index + 1;
    end

    % the rest of the function follows
end
\end{lstlisting}
