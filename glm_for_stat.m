%GLM_FOR_STAT   Fits a GLM to predict an output statistic from some parameters.
%   For some partition of an LHS experiment, GLM_FOR_STAT fits a generalised
%   linear model to predict an output statistic from the values of the model
%   parameters that are most highly correlated with the output statistic.
%
%   [b dev names rho stats] = GLM_FOR_STAT(lhs_data, stat, pre_process_fn, threshold,
%   'PropertyName', PropertyValue);
%
%   LHS_DATA is the LHS experiment from which the GLM will be derived.
%
%   STAT is the name or index of an output statistic for the LHS experiment.
%
%   THRESHOLD determines how many model parameters should be included as
%   observations for the GLM. If THRESHOLD is an integer, it specifies the
%   exact number of model parameters; if THRESHOLD is between 0 and 1, it
%   specifies all model parameters whose rank coefficient is larger than
%   THRESHOLD.
%
%   'distr' defines the probability distribution for fitting the GLM; the
%   default value is 'normal' (see GLMFIT for more information).
%
%   'link' defines the link function for fitting the GLM; the default value is
%   'identity' (see GLMFIT for more information).
%
%   'pre_process_fn' is a pre-processing function to apply to the output
%   statistic to ensure it is a scalar. The result is passwd to 'fn' for
%   further processing. The default value is to take the mean over the
%   statistic (which is no transformation for scalar statistics), but
%   a warning is generated if this is not specified for
%   non-scalar data.
%
%   'fn' defines a transformation to apply to the output statistic; the default
%   value is to perform no transformation.
%
%   'partition' defines the partition of the LHS simulations to which the GLM
%   will be fitted; the default value is all of the LHS simulations.
%
%   'dependent_params' is a cell array containing the names of any dependent
%   model parameters; the default value is an empty cell array (ie, no
%   dependent model parameters).
%
%   B is a vector of coefficient estimates.
%
%   DEV is the deviance of the fit, which is a generalisation of the residual
%   sum of squares.
%
%   NAMES is a cell array containing the names of the parameters that have been
%   chosen as observations for the GLM.
%
%   RHO is a vector of the estimated rank coefficients for the observation
%   parameters.
%
%   STATS is a structure that provides more details about the GLM (see GLMFIT
%   for more information).
%
%See also glmfit, glm_deviance, glm_roc

% Copyright 2010, 2011 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [b dev names rho stats] = glm_for_stat(lhs_data, stat, threshold, varargin)
    p = inputParser();
    p.addRequired('lhs_data', @isstruct);
    p.addRequired('stat', @(x) ischar(x) || (isscalar(x) && x > 0));
    p.addRequired('threshold', @(x) isscalar(x) && x > 0);
    p.addParamValue('distr', 'normal', @ischar);
    p.addParamValue('link', 'identity', @ischar);
    p.addParamValue('pre_process_fn', @(x) mean(x,3));
    p.addParamValue('fn', @(x) x);
    p.addParamValue('partition', 0);
    p.addParamValue('dependent_params', {});
    p.parse(lhs_data, stat, threshold, varargin{:});

    % Generate a warning if statistic is a vector and pre_process_fn was
    % not specified
    temp_var = layout_rec(lhs_data.stat_layout,lhs_data.stats);
    if ((length(temp_var.(stat)) ~= 1) && max(strcmp(p.UsingDefaults,'pre_process_fn')))
        fprintf(1,'WARNING: Statistic %s is a vector and ''pre_process_fn'' not specified.\n', stat)
        fprintf(1,'         Default of taking the mean has been used which may or may not be appropriate.\n')
    end
    
    if p.Results.partition == 0
        [rho , ~, param_inds param_vals stat_vec] = ...
            rank_corr(lhs_data, stat, p.Results.pre_process_fn, p.Results.fn, p.Results.dependent_params);
    else
        [rho , ~, param_inds param_vals stat_vec] = ...
            rank_corr(lhs_data, stat, p.Results.pre_process_fn, p.Results.fn, p.Results.dependent_params, ...
            p.Results.partition);
    end
    % ignore dependent and constant parameters
    rho = rho(param_inds);
    param_vals = param_vals(:, param_inds);
    param_names = fieldnames(lhs_data.parameter_samples);
    param_names = param_names(param_inds);
    % apply any required transformation of the output statistic
    stat_vec = p.Results.pre_process_fn(stat_vec);
    stat_vec = p.Results.fn(stat_vec);

    if threshold >= 1
        % Specifies the top N parameters
        [~, sorted_ix] = sort(abs(rho), 'descend');
        params_ix = sorted_ix(1:threshold);
    elseif threshold > 0
        % Specifies parameters with a coefficient > N
        params_ix = find(rho >= threshold);
    else
        error('glm_for_stat:threshold', 'Invalid threshold');
    end

    % take the chosen observations from the parameter values
    glm_values = param_vals(:, params_ix);
    names = param_names(params_ix);
    rho = rho(params_ix);
    % fit the GLM to the (transformed) statistic, given the chosen observations
    [b dev stats] = glmfit(glm_values, stat_vec', ...
        p.Results.distr, 'link', p.Results.link);
end
