%LHS   Perform a set of model simulations using Latin Hypercube Sampling (LHS).
%   For a given model and probability distributions for any of the model
%   parameters, LHS randomly samples points from the model parameter space in a
%   biased manner, to ensure that the set of random points are representative
%   of the real variability (assuming, of course, that the given probability
%   distributions are reasonably accurate).
%
%   mat_file = LHS(model, param_dists, samples, control_params);
%
%   MODEL is a record (structure array) that contains the model details (see
%   EXAMPLE_SIR_MODEL for an example of such a model).
%
%   PARAM_DISTS is a record (structure array) that contains probability
%   distributions for the model parameters that will be sampled at random (see
%   EXAMPLE_LHS_EXPERIMENT for an example).
%
%   SAMPLES is the number of samples to take from the parameter space (and
%   therefore the number of simulations to perform) for each combination of the
%   control parameters.
%
%   CONTROL_PARAMS is any number of control parameters, each defined as a
%   single record (structure array) that contains the name of the model
%   parameter and a list of the values that the parameter will take (see
%   EXAMPLE_LHS_EXPERIMENT for an example).
%
%   MAT_FILE is the name of the .mat file that contains the results of the LHS
%   experiment.
%
%   Example:
%      >> model = example_SIR_model();
%      >> param_dists = model.default_dists();
%      >> control = struct('name', 'alpha', ...
%            'description', 'Does something', ...
%            'values', linspace(0, 1, 11));
%      >> mat_file = lhs(model, param_dists, 100, control);
%
%See also example_SIR_model, example_LHS_experiment, lhs_resume

% Copyright 2010, 2012, 2013 Robert Moss, James McCaw, Patricia Campbell.
% SPDX-License-Identifier: GPL-3.0-or-later
function [mat_file] = lhs(model, param_dists, samples, varargin)

    % store the state of the LHS experiment in a record for easy snapshots
    ss = struct('model', model, 'param_dists', param_dists, ...
        'samples', samples, 'cpars', {varargin});

    % ensure that the necessary model fields exist
    reqd = {'statistics', 'params', 'state', 'equations'};
    check_fields(reqd, ss.model, 'model');

    % determine the number of control parameters
    ss.cpar_count = length(ss.cpars);
    % ensure that there is at least one control parameter
    if ss.cpar_count == 0
        error('lhs:nocontrols', 'no control parameters');
    end

    % ensure that the necessary control parameter fields exist
    for i = 1:ss.cpar_count
        reqd = {'name', 'values'};
        check_fields(reqd, ss.cpars{i}, 'control parameter');
    end

    % If no 'description' field exists for a control parameter,
    % warn the user and set the description to the parameter name.
    for i = 1:ss.cpar_count
        if ~ isfield(ss.cpars{i}, 'description')
            warning('lhs:nodescription', ...
                'No description for control parameter "%s"', ss.cpars{i}.name);
            ss.cpars{i}.description = ss.cpars{i}.name;
        end
        reqd = {'name', 'values'};
        check_fields(reqd, ss.cpars{i}, 'control parameter');
    end

    % use a private stream of random numbers,
    % a separate stream is used for each of
    % 1. parameter permutation
    % 2. independent parameter sampling
    % 3. dependent parameter sampling
    % 4. potentially, ODE RHS stochastic model calls
    [ss.rs_val ss.rs_depval, ss.rs_perm, ss.rs_depperm, ss.rs_sim] = ...
        RandStream.create('mrg32k3a', 'Seed', 3136, 'NumStreams', 5);

    % determine the number of values for each control parameter
    ss.cpar_size = cell([1 ss.cpar_count]);
    ss.cpar_values = cell([1 ss.cpar_count]);
    ss.cpar_names = cell([1 ss.cpar_count]);
    for i = 1:ss.cpar_count
        ss.cpar_size{i} = length(ss.cpars{i}.values);
        ss.cpar_values{i} = ss.cpars{i}.values;
        ss.cpar_names{i} = ss.cpars{i}.name;
    end

    % determine the number of statistics returned for each simulation
    ss.stat_names = model.statistics([],model.params());
    if iscellstr(ss.stat_names)
        ss.stat_count = sum(cellfun(@(x) cell2mat(x(2)), ss.stat_names));
    elseif isstruct(ss.stat_names)
        ss.stat_layout = layout_of(ss.stat_names);
        ss.stat_names = fieldnames(ss.stat_names);
        ss.stat_count = ss.stat_layout.size;
    else
        error('lhs:statistics', 'Invalid data format for model.statistics()');
    end
    % statistics are stored in a matrix
    ss.stats = zeros(ss.cpar_size{:}, ss.samples, ...
        ss.stat_count);
    % statistics for each parallel loop are stored separately
    ss.local_stats = zeros(ss.samples, ss.stat_count);

    % determine the layout of the independent parameters (ie, how to convert
    % the parameter values into a single column vector)
    layout_indep_params = layout_of(ss.param_dists);
    ss.ind_names = fieldnames(ss.param_dists);
    % ensure that none of the independent parameters are attempting to override
    % a dependent parameter, because the dependent parameters take precedence
    if isfield(ss.model, 'dependent_dists')
        dep_dists = ss.model.dependent_dists(model.params());
    else
        dep_dists = struct();
    end
    ss.dep_names = fieldnames(dep_dists);
    if any(ismember(ss.ind_names, ss.dep_names))
        warning('lhs:dists', ...
            'Cannot adjust dependent parameters in an LHS experiment');
        ss.ind_names{ismember(ss.ind_names, ss.dep_names)}
    end
    % determine the layout of the dependent parameters (ie, how to convert the
    % parameter values into a single column vector)
    dep_details = structfun(@(dist) dist(), dep_dists, 'UniformOutput', 0);
    layout_depen_params = layout_of(dep_details);
    % determine the layout of all parameters that are sampled for each
    % simulation (ie, how to record the chosen parameter values)
    ss.param_names = [ss.ind_names; ss.dep_names];
    ss.sample_layout = layout_merge(layout_indep_params, layout_depen_params);

    % permute the sequence of sub-spaces for each parameter, to ensure that the
    % chosen samples cover them in a random order (this includes all dependent
    % parameters); each parameter space sample is recorded in chosen_samples
    ss.chosen_samples = struct();
    ss.sample_subspace = struct();
    
    % Get a local copy of the parameters for space allocation calculations
    parameters = model.params();
    for param = 1:length(ss.param_names)
        pname = ss.param_names{param};
        dist_info = ss.sample_layout.(pname);
        dist_count = dist_info(1);
        % allocate space to store all sampled values for the parameter
        % For vectorized parameters with dist_count = 1 (ie a single
        % distribution for all values of the parameter) we must ensure
        % chosen_samples has the appropriate space allocated.
        if isfield(parameters,pname)
            space = numel(parameters.(pname));
        else
            % Take care, we don't know what size this pname requires.
            % We need to call the dep_dists function and take the size of
            % the result
            pdist = dep_dists.(pname);
            % choose each value at random from the given distribution
            if isa(pdist,'function_handle')
                space = numel(pdist(0));
            else
                % Handle vector and matrix parameter distributions
                space = numel(pdist);
            end
        end
        ss.chosen_samples.(pname) = zeros(ss.cpar_size{:}, ss.samples, space);
        % permute the sub-spaces for each distribution for the parameter
        ss.sample_subspace.(pname) = zeros(ss.samples, dist_count);
        
        % set random number stream appropriately for indep or dep dists
        if isfield(dep_dists,pname)
            rs = ss.rs_depperm;
        else
            rs = ss.rs_perm;
        end
        for i = 1:dist_count
            ss.sample_subspace.(pname)(:, i) = randperm(rs, ss.samples);
        end
    end
    % samples for each parallel loop are stored separately
    ss.local_samples = cell([ss.samples 1]);

    % pre-select the random values for each sub-space
    % (ie, the same samples are used for all values of the control parameters)
    ss.rand_vals = cell(ss.samples, layout_indep_params.size);
    fprintf(1, '\rHypercube: initialising samples (%d)\n', ss.samples);
    cumulative_offset = zeros(1,length(ss.ind_names));
    for param = 1:length(ss.ind_names)
        pname = ss.ind_names{param};
        cumulative_offset(param) = layout_indep_params.(pname)(1);
    end
    cumulative_offset = [0 cumsum(cumulative_offset(1:end-1))];
    % Reverse loop ordering here to go through samples inside of
    % parameters.
    % Otherwise our random number selections for parameters changes if the
    % number of parameters changes (e.g. if including samples for
    % intervention parameters).
    %for s = 1:ss.samples
        % choose a random value for each parameter
    for param = 1:length(ss.ind_names)
        for s = 1:ss.samples
            %for param = 1:length(ss.ind_names)
            pname = ss.ind_names{param};
            samples = ss.sample_subspace.(pname)(s,:);
            if isa(ss.param_dists.(pname),'function_handle')
                pvalues = lhs_rand(ss, ss.param_dists.(pname), samples, 'rs_val');
                ss.rand_vals{s, 1+cumulative_offset(param)} = pvalues;
            else
                for i = 1:numel(ss.param_dists.(pname))
                    pvalues = lhs_rand(ss, ss.param_dists.(pname){i}, samples(i), 'rs_val');
                    ss.rand_vals{s, i+cumulative_offset(param)} = pvalues;
                end
            end
        end
    end

    % the experiment name is the filename from which lhs() was called
    ST = dbstack();
    [junk, exp_name] = fileparts(ST(2).file);
    ss.exp_name = exp_name;

    %% main control loop
    % for each combination of the control parameters, perform the simulations
    % for all of the random samples from the hypercube of parameters
    ss.control_param_vals = combvec(ss.cpar_values{:});
    ss.step_count = length(ss.control_param_vals);
    ss.step_num = 0;
    ss.cpar_inds = cell([1 ss.cpar_count]);
    ss.init_params = ss.model.params();
    ss.tmp_file = filename('data', ['tmp_' ss.exp_name], '.mat', 1);

    % begin the simulations
    [mat_file] = lhs_resume(ss);
end

%CHECK_FIELDS   Ensure that a record (structure array) has the required fields.
%   Given a record and a cell array containing the name of required fields,
%   CHECK_FIELDS raises an exception if any of the required fields are not
%   present.
%
%   check_fields(reqd_fields, record, description);
%
%   REQD_FIELDS is a cell array that contains the names of the required fields.
%
%   RECORD is the record (structure arrays) whose fields are to be checked.
%
%   DESCRIPTION is a descriptive name for RECORD, which is used to generate a
%   (hopefully) meaningful error message if any required fields are missing.
%
function check_fields(reqd_fields, record, description)
    mask = isfield(record, reqd_fields);
    if ~ all(mask)
        missing = reqd_fields(mask == 0);
        missing = cellfun(@(x) [' ' x], missing, 'UniformOutput', 0);
        error('lhs:missingfields', ...
            '%s is missing required fields:%s.', description, [missing{:}]);
    end
end
