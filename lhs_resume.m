%LHS_RESUME   Resume an interrupted LHS experiment.
%   If an LHS experiment should terminate early (eg, due to power loss),
%   LHS_RESUME can resume the experiment from the most recent checkpoint.
%
%   mat_file = lhs_resume(experiment_name);
%
%   EXPERIMENT_NAME can either be the name of the experiment that terminated
%   early, or a snapshot of an LHS experiment.
%
%   MAT_FILE is the name of the .mat file that contains the results of the LHS
%   experiment.
%
%   Example:
%      % Resume an experiment from a recorded snapshot
%      >> filedata = load(lhs_snapshot_mat_file);
%      >> mat_file = lhs_resume(filedata.lhs_snapshot);
%      % Resume another experiment by specifying the experiment name
%      >> mat_file = lhs_resume('some_experiment_name');
%
%See also lhs

% Copyright 2010-2016 Robert Moss, James McCaw, Patricia Campbell.
% SPDX-License-Identifier: GPL-3.0-or-later
function [mat_file] = lhs_resume(ss)

    if ischar(ss)
        % if an experiment name has been provided, then
        % load the most recent snapshot of the experiment
        try
            [filedata, ~, fullpath] = most_recent(['tmp_' ss]);
        catch exception
            if strcmp('most_recent:none', exception.identifier)
                % report a more specific error -- no snapshot exists
                error('lhs_resume:none', 'No snapshot for experiment "%s"', ss);
            else
                % some other error has occurred, don't interfere with it
                rethrow(exception);
            end
        end
        if ~ isfield(filedata, 'lhs_snapshot')
            % check whether the file contains a valid LHS snapshot
            error('lhs_resume:invalid', ...
                'Invalid snapshot for experiment "%s"', ss);
        end
        ss = filedata.lhs_snapshot;
        % the location of the snapshot file may have changed
        ss.tmp_file = fullpath;
    elseif ~ isstruct(ss)
         error('lhs_resume:wrongarg', ...
             'only strings and structures are accepted');
    end

    % display the snapshot filename so that the simulation
    % can be easily resumed if it is terminated early
    fprintf(1, 'snapshot file: ''%s''\n', ss.tmp_file);

    % select the remaining combinations of control parameter values
    control_param_vals = ss.control_param_vals(:, ss.step_num + 1:end);

    for i = control_param_vals
        % print the progress of the hypercube sampling
        c = clock;
        fprintf(1, 'Hypercube[%d/%d @ %02d:%02d]\n', ...
            ss.step_num, ss.step_count, c(4), c(5));

        % use local variables for storing parameter samples and statistics
        local_samples = ss.local_samples;
        local_stats = ss.local_stats;

        layout_indep_params = layout_of(ss.param_dists);
        
        % progress recording
        tmp_fname = tempname;
        lhs_parfor_progress(tmp_fname,ss.samples); % initialise progress recording
        
        % Set the global random number stream to ss.rs_sim
        prev_stream = RandStream.setGlobalStream(ss.rs_sim);
        
        % perform a simulation for each sample of the parameter space
        parfor s = 1:ss.samples
            parameters = struct(ss.init_params);
            % set the random values of the independent parameters
            
            cumulative_offset = zeros(1,length(ss.ind_names));
            for param = 1:length(ss.ind_names)
                pname = ss.ind_names{param};
                cumulative_offset(param) = layout_indep_params.(pname)(1);
            end
            cumulative_offset = [0 cumsum(cumulative_offset(1:end-1))];
            
            for param = 1:length(ss.ind_names)
                pname = ss.ind_names{param};
                if isa(ss.param_dists.(pname),'function_handle')
                    rnd_sz = size(ss.rand_vals{s, 1+cumulative_offset(param)});
                    exp_sz = size(parameters.(pname));
                    if rnd_sz == exp_sz
                        parameters.(pname) = ss.rand_vals{s, 1+cumulative_offset(param)};
                    else
                        parameters.(pname) = repmat( ...
                            ss.rand_vals{s, 1+cumulative_offset(param)}, ...
                            size(parameters.(pname)));
                    end
                else
                   % Handle vector and matrix parameter distributions
                   all_rand_vals = cell2mat(ss.rand_vals(s,:));
                   the_rand_vals = all_rand_vals((1+cumulative_offset(param)):(cumulative_offset(param)+numel(parameters.(pname))));
                   the_rand_vals = reshape(the_rand_vals,size(parameters.(pname),1),size(parameters.(pname),2));
                   parameters.(pname) = the_rand_vals;
                end
            end
            
            % set the value of the control parameters
            for cv = 1:ss.cpar_count
               cv_name = ss.cpar_names{cv};
               parameters.(cv_name) = i(cv);
            end
            % set the random values of the dependent parameters (if any)
            if isfield(ss.model, 'dependent_dists')
                dep_dists = ss.model.dependent_dists(parameters);
                for param = 1:length(ss.dep_names)
                    pname = ss.dep_names{param};
                    pdist = dep_dists.(pname);
                    if isa(pdist,'function_handle')
                        parameters.(pname) = ...
                            lhs_rand(ss, pdist, ss.sample_subspace.(pname)(s,:), 'rs_depval');
                    else
                        % Handle vector and matrix parameter distributions
                        for j = 1:size(pdist,2)
                            for k = 1:size(pdist,1)
                                parameters.(pname)(k,j) = lhs_rand(ss, pdist{k,j}, ...
                                    ss.sample_subspace.(pname)(s,k+j-1), 'rs_depval');
                            end
                        end
                    end
                end
            end
      
            % perform the simulation
            [time, final_state, events] = solve(ss.model, parameters);
            % record the relevant statistics
            stats = ss.model.statistics(time, parameters, final_state, events);
            if isfield(ss, 'stat_layout')
                stats = layout_vec(ss.stat_layout, stats);
            end
            local_stats(s, :) = stats;

            % record the chosen sample of the parameter space
            local_samples{s} = struct();
            for param = 1:length(ss.param_names)
                pname = ss.param_names{param};
                local_samples{s}.(pname) = parameters.(pname);
            end
            
            % report progress
            lhs_parfor_progress(tmp_fname);

        end
        % clean up
        lhs_parfor_progress(tmp_fname,0);
        
        % determine the index of each control parameter value
        for cv = 1:ss.cpar_count
           ss.cpar_inds{cv} = find(ss.cpar_values{cv} == i(cv));
        end

        % record the statistics for each simulation
        ss.stats(ss.cpar_inds{:}, :, :) = local_stats(:, :);

        % record all of the parameter space samples
        for s = 1:ss.samples
            for param = 1:length(ss.param_names)
                pname = ss.param_names{param};
                ss.chosen_samples.(pname)(ss.cpar_inds{:}, s, :) = ...
                    reshape(local_samples{s}.(pname), ...
                    numel(local_samples{s}.(pname)),1);
            end
        end

        % keep track of how many steps have been performed
        ss.step_num = ss.step_num + 1;
        
        % save a snapshot in a temporary .mat file
        lhs_snapshot = ss;
        save('-v7.3', ss.tmp_file, 'lhs_snapshot');
        clear('lhs_snapshot');
    end
    
    % Restore the global random number stream
    RandStream.setGlobalStream(prev_stream);
    
    % print the final progress marker (ie, completion)
    c = clock;
    fprintf(1, 'Hypercube[%d/%d @ %02d:%02d]\n', ...
        ss.step_count, ss.step_count, c(4), c(5));

    % save the results
    mat_file = filename('data', ss.exp_name, '.mat', 1);
    lhs_data = struct( ...
        'param_dists', ss.param_dists, 'samples', ss.samples, ...
        'stat_names', {ss.stat_names}, 'stats', ss.stats, ...
        'parameter_samples', ss.chosen_samples, ...
        'control_params', {ss.cpars}, 'exp_name', ss.exp_name, ...
        'mat_file', mat_file ...
        );
    % also save the "unsampled" parameters unless excluded by
    % model.unsaved_params()
    all_pnames = fieldnames(ss.model.params());
    all_pnames_vals = ss.model.params();
    if isfield(ss.model, 'unsaved_params')
        % Add in all except those.
        for i = 1:length(all_pnames)
            if ~(isfield(ss.chosen_samples, all_pnames(i)) || ...
                    any(cellfun(@(x) strcmp(x,all_pnames(i)),ss.model.unsaved_params())))
                lhs_data.unsampled_params.(cell2mat(all_pnames(i))) = ...
                    all_pnames_vals.(cell2mat(all_pnames(i)));
            end
        end
    else
        % Add in all
        for i = 1:length(all_pnames)
            if ~isfield(ss.chosen_samples, all_pnames(i))
                lhs_data.unsampled_params.(cell2mat(all_pnames(i))) = ...
                    all_pnames_vals.(cell2mat(all_pnames(i)));
            end
        end
    end
    
    % also save the layout of the statistics, if present
    if isfield(ss, 'stat_layout')
        lhs_data.stat_layout = ss.stat_layout;
    end
    save('-v7.3', mat_file, 'lhs_data');

    % remove the temporary snapshot file
    delete(ss.tmp_file);
    
end
