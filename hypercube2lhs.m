%HYPERCUBE2LHS   Converts older experimental results into LHS form.
%   Older LHS code stored experimental results differently; notably, results
%   were saved in structures called 'hypercube_data' or 'hypercube2_data'
%   (depending on whether there were one or two control parameters). This
%   function converts those results into the form that the rest of the LHS
%   framework understands. However, it is advisable to rewrite the experiment
%   to use the LHS framework and then run it again, rather than using this
%   function.
%
%   This function will fail if the results do not include the parameter values
%   for each sample in the LHS experiment (the field 'parameter_samples'). A
%   simple workaround is to re-run the experiment and record the parameter
%   values. This works because the random number generator is always seeded
%   with the same value and will therefore choose the same parameter values.
%
%   NOTE: the probability distributions associated with the model parameters
%   will not convert correctly, since the way that probability distributions
%   are wrapped by the LHS framework (see DIST_WRAPPER) is different to the old
%   method. Such a conversion is not impossible, but it is tedious and of no
%   immediate value, since the distributions can be obtained directly from the
%   original M-files.
%
%   lhs_data = HYPERCUBE2LHS(data);
%
%   DATA is the results of an older LHS experiment (that is, hypercube_data or
%   hypercube2_data).
%
%   LHS_DATA contains the same results, but in a form that the LHS framework
%   accepts.
%
%   Example:
%      >> lhs_data = hypercube2lhs( most_recent('some_old_experiment') );
%
%See also lhs

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function lhs_data = hypercube2lhs(data)
    if ~ isfield(data, 'parameter_samples')
        error('hypercube2lhs:data', 'Field "parameter_samples" is missing');
    end

    lhs_data = struct();
    lhs_data.param_dists = data.param_dists;
    lhs_data.samples = data.samples;
    lhs_data.stat_names = data.result_name;
    lhs_data.stats = data.results;
    lhs_data.parameter_samples = data.parameter_samples;
    lhs_data.exp_name = data.exp_name;
    lhs_data.mat_file = data.mat_file;

    if isfield(data, 'param_vals')
        param = struct('name', data.display_name, ...
            'description', data.display_name, ...
            'values', data.param_vals);
        cpars = {param};
    elseif isfield(data, 'p1_vals')
        param1 = struct('name', data.p1_disp, ...
            'description', data.p1_disp, ...
            'values', data.p1_vals);
        param2 = struct('name', data.p2_disp, ...
            'description', data.p2_disp, ...
            'values', data.p2_vals);
        cpars = {param1 param2};
    else
        error('hypercube2lhs:data', 'No control parameters found');
    end
    lhs_data.control_params = cpars;
end
