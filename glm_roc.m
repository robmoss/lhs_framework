%GLM_ROC   Plot the ROC curve for a GLM.
%   The more the curve hugs the left and top edges of the plot (ie, the closer
%   the area under the curve is to 1), the better the GLM.
%
%   [tpr fpr thresholds] = GLM_ROC(lhs_data, names, stat, b, 'PropertyName',
%   PropertyValue);
%
%   LHS_DATA is the LHS experiment from which the GLM was derived and to which
%   it will be compared.
%
%   NAMES is a cell array containing the names of the parameters that have been
%   chosen as observations for the GLM.
%
%   STAT is the name or index of an output statistic for the LHS experiment.
%
%   B is the vector of coefficient estimates for the GLM.
%
%   'link' defines the link function that was used to fit the GLM; the default
%   value is 'identity' (see GLMFIT for more information).
%
%   'pre_process_fn' is a pre-processing function to apply to the output
%   statistic to ensure it is a scalar. The result is passwd to 'fn' for
%   further processing. The default value is to take the mean over the
%   statistic (which is no transformation for scalar statistics), but
%   a warning is generated if this is not specified for
%   non-scalar data.
%
%   'fn' is the transformation to apply to the output statistic before
%   comparing the output statistic to values returned by the GLM; the default
%   value is to perform no transformation.
%
%   Example:
%      % Randomly partition the experiment into seven partitions
%      >> parts = partition_randomly(lhs_data, 7);
%      % A binary function of attack rate, 1 means the epidemic was mitigated
%      >> fn = @(x) 0 + (x < 0.1);
%      % Derive a 4-parameter GLM for the attack rate, fitted to partition 5
%      >> [b dev names rho stats] = glm_for_stat(lhs_data, 'Attack Rate', 4,
%            'partition', parts == 5, 'distr', 'binomial', 'link', 'logit',
%            'fn', fn);
%      % Plot the ROC curve of how well the GLM predicts the recorded results
%      >> [tpr fpr thresholds] = glm_roc(lhs_data, names, 'Attack Rate', b,
%            'link', 'logit', 'fn', fn);
%
%See also glm_for_stat, glm_deviance

% Copyright 2010, 2011 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [tpr fpr thresholds] = glm_roc(lhs_data, names, stat, b, varargin)
    p = inputParser();
    p.addRequired('lhs_data', @isstruct);
    p.addRequired('names', @iscellstr);
    p.addRequired('stat', @ischar);
    p.addRequired('b', @isnumeric);
    p.addParamValue('link', 'identity', @ischar);
    p.addParamValue('pre_process_fn', @(x) mean(x,3));
    p.addParamValue('fn', @(x) x);
    p.parse(lhs_data, names, stat, b, varargin{:});

    % Generate a warning if statistic is a vector and pre_process_fn was
    % not specified
    temp_var = layout_rec(lhs_data.stat_layout,lhs_data.stats);
    if ((length(temp_var.(stat)) ~= 1) && max(strcmp(p.UsingDefaults,'pre_process_fn')))
        fprintf(1,'WARNING: Statistic %s is a vector and ''pre_process_fn'' not specified.\n', stat)
        fprintf(1,'         Default of taking the mean has been used which may or may not be appropriate.\n')
    end
    
    % produce a single partition (ie, the whole data set)
    part = partition_randomly(lhs_data, 1);
    % retrieve the observations for the GLM
    obs_mat = partition_parameters(lhs_data, part == 1, names);
    % calculate the GLM predictions for these observations
    glm_values = glmval(b, obs_mat, p.Results.link);
    % collect the output statistic
    stat_vec = p.Results.fn( partition_statistic(lhs_data, part == 1, stat) );
    % calculate the true-positive and false-positive ratios
    [tpr fpr thresholds] = roc(stat_vec , glm_values');

    % plot the ROC curve and estimate the integral
    figure();
    fill_color = [0.33 0.5 0.9];
    line_color = 0.5 * fill_color;
    area = trapz(fpr, tpr);
    % plot the ROC curve itself
    plot(fpr, tpr, 'LineWidth', 4, 'Color', line_color);
    hold on;
    % plot the dashed line y = x (the worst possible model)
    plot(gca, [0 1], [0 1], '--', 'LineWidth', 2, 'Color', [0 0 0]);
    hold off;
    % display the area under the ROC curve on the graph
    text(0.55, 0.35, sprintf('Area = %0.3f', area), ...
        'Color', [0 0 0], ...
        'FontSize', 24, 'FontWeight', 'bold');
    % tailor the graph for presentations
    set(gca, 'fontsize', 24);
    set(gca, 'XTick', [0 0.2 0.4 0.6 0.8 1.0]);
    set(gca, 'YTick', [0 0.2 0.4 0.6 0.8 1.0]);
    set(gca, 'ZTick', [0 0.2 0.4 0.6 0.8 1.0]);
    % set axis labels
    xlabel('1 - Specificity');
    ylabel('Sensitivity');
end
