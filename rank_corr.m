%RANK_CORR   Calculates rank coefficients between parameters and an output.
%   Given a partition of an LHS experiment, RANK_CORR calculates the rank
%   (Spearman) coefficient between each independent (non-constant) model
%   parameter and a single output statistic. If no output arguments are
%   specified, it plots these coefficients as a bar chart.
%
%   [rho pval param_inds param_vals stat_vec] = rank_corr(lhs_data, stat,
%   dependent_params, partition);
%
%   LHS_DATA is the LHS experiment for which the rank coefficients will be
%   calculated.
%
%   STAT is the name or index of an output statistic for the LHS experiment.
%
%   PRE_PROCESS_FN defines a transformation to apply to the output
%   statistic to produce a scalar value. See glm_for_stat.m and glm_roc.m
%   for further details.
%
%   DEPENDENT_PARAMS is a cell array that contains the names of all dependent
%   parameters in the model, which will be ignored. If this argument is not
%   specified, the default value is an empty cell array.
%
%   PARTITION identifies a partition of the LHS simulations for which the rank
%   coefficients will be calculated. If this argument is not specified, the
%   default value is to consider all of the LHS simulations.
%
%   RHO is a vector of rank coefficients, where dependent and constant
%   parameters are assigned a coefficient of NaN.
%
%   PVAL is a vector of p-values for testing the hypothesis of no correlation
%   against the alternative that there is a nonzero correlation.
%
%   PARAM_INDS is a vector of parameter indices that identifies the
%   independent, non-constant model parameters.
%
%   PARAM_VALS is a matrix of parameter values for the independent,
%   non-constant model parameters.
%
%   STAT_VEC is a vector containing the values of the output statistic for the
%   chosen partition of LHS simulations.
%
%   Example:
%      % Calculate coefficients and p-values for the 'Attack Rate' statistic
%      >> [rho pval] = rank_corr(lhs_data, 'Attack Rate');
%      % Plot the coefficients for the first output statistic
%      >> rank_corr(lhs_data, 1);

% Copyright 2010, 2011 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [rho pval param_inds param_vals stat_vec] = rank_corr(lhs_data, stat, pre_process_fn, fn, dependent_params, partition)
    param_names = fieldnames(lhs_data.parameter_samples);
    param_count = length(param_names);
    lhs_dims = size(lhs_data.parameter_samples.(param_names{1}));
    if nargin < 6
        partition = true(lhs_dims);
    end

    stat_count = length(lhs_data.stat_names);

    if ischar(stat)
        [~, index] = ismember(stat, lhs_data.stat_names);
        if index < 1
            error('correlations:stat', 'Invalid statistic "%s"', stat);
        end
    elseif isscalar(stat) && isnumeric(stat)
        if stat > 0 && stat <= stat_count
            index = stat;
        else
            error('correlations:stat', 'Invalid statistic "%f"', stat);
        end
    else
        error('correlations:stat', 'Invalid statistic parameter');
    end

    range = @(len) 1:len;
    stat_inds = arrayfun(range, lhs_dims, 'UniformOutput', 0);
        
    seq_of_entries = structfun(@(x) x(1), lhs_data.stat_layout);
    cum_seq_of_entries = [0 ; cumsum(seq_of_entries(1:end-1))];
    stat_entries = (1:seq_of_entries(index)) + cum_seq_of_entries(index);
        
    stat_mat = lhs_data.stats(stat_inds{:}, stat_entries);
    stat_vec = stat_mat(1,partition,:);
    stat_vec = pre_process_fn(stat_vec);
    stat_vec = fn(stat_vec);
    entries = nnz(partition);
    
    param_vals = zeros([entries param_count]);
    
    % arrange the parameter values accordingly
    param_inds = [];
    for i = 1:param_count
        pname = param_names{i};
        pvals = lhs_data.parameter_samples.(pname);
        pvals = pvals(partition);
        range = max(pvals) - min(pvals);
        if range > 0 && ~ ismember(pname, dependent_params)
            % record which independent parameters vary
            param_inds = [param_inds; i];
            param_vals(:, i) = pvals;
        else
            % ignore constant and dependent parameters
            param_vals(:, i) = NaN;
        end
    end
    
    % calculate the rank correlation between each parameter and the statistic
    [rho pval] = corr(param_vals, stat_vec', 'type', 'Spearman');

    % plot the correlations as a bar graph, if there are no output arguments
    if nargout > 0
        return;
    end

    % ignore dependent variables
    param_vals = param_vals(:,param_inds);
    param_names = param_names(param_inds);
    param_count = length(param_inds);
    rho = rho(param_inds);
    pval = pval(param_inds);

    % collect all of the negative terms (and make them positive)
    neg_rhos = - rho;
    neg_rhos(rho > 0) = 0;
    % collect all of the positive terms
    pos_rhos = rho;
    pos_rhos(rho < 0) = 0;
    % plot with different colours for positive and negative values
    bar([pos_rhos neg_rhos], 'stacked');
    legend('Positive', 'Negative');
    % set the labels on the x-axis to include the p-values
    for i = 1:param_count
        if pval(i) < 0.001
            pval_str = ' (P < 0.001)';
        else
            pval_str = sprintf(' (P = %.3f)', pval(i));
        end
        param_names{i} = [param_names{i} pval_str];
    end
    set(gca, 'XTick', 1:param_count);
    set(gca, 'XTickLabel', param_names);
    ylabel(sprintf('Correlation with %s', stat));
end
