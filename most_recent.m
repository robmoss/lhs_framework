%MOST_RECENT   Loads the most recent results for a given experiment.
%   Loads the most recent results (.mat file) for the experiment whose name
%   matches the specified pattern. The optional parameter |nth_last| can be
%   used to open a less-recent result.
%
%   [data, filename, fullpath] = most_recent(experiment, nth_last);
%
%   EXPERIMENT is the name of the LHS experiment.
%
%   NTH_LAST (optionally) specifies to load the nth-most recent results.
%
%   DATA is the contents of the .mat file (typically a single structure
%   containing the details of a LHS experiment).
%
%   FILENAME is the name of the .mat file from which the results were taken,
%   without the .mat extension.
%
%   FULLPATH is the absolute path of the .mat file.
%
%   Example:
%      % Loads the third-last run of the experiment 'some_exp'
%      >> data = most_recent('some_exp', 3);
%
%See also filename, filename_timestamped

% Copyright 2010-2016 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [data, matfile, fullpath] = most_recent(experiment, nth_last, quiet)
    % default to returning the most recent result
    if nargin < 2
        nth_last = 1;
    end
    % default to printing the file name
    if nargin < 3
        quiet = 0;
    end

    % find the path to the directory that contains the experimental data
    data_dir = filename('data');
    % return the list of all matching experimental results
    % NOTE: the '_20*' is necessary to distinguish between, say,
    % exp1_2010-01-01_12-00.mat and exp1_someflag_2010-01-01_12-00.mat
    files = dir(fullfile(data_dir, [experiment '_20*.mat']));

    % index the files by date
    dates = zeros(1, length(files));
    for i = 1:length(files)
        dates(i) = files(i).datenum;
    end
    % sort the results in descending order (ie, most recent first)
    [~, Is] = sort(dates, 'descend');

    % ensure that nth_last points to a valid result
    max_nth_last = length(Is);
    if nth_last > max_nth_last && max_nth_last > 0
        nth_last = max_nth_last;
        warning('most_recent:nth_last', 'nth_last reduced to %d', nth_last);
    end
    if length(Is) < 1
        error('most_recent:none', 'No data for experiment "%s"', experiment);
    end

    % Load the experimental results and return the data
    name = files(Is(nth_last)).name;
    if quiet == 0
        fprintf(1, '%s\n', name); % print the filename for diagnostic purposes
    end
    fullpath = fullfile(data_dir, name);
    data = load(fullpath);
    [~, matfile] = fileparts(name);
end
