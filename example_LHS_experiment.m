%EXAMPLE_LHS_EXPERIMENT   An example of a LHS experiment for a SIR model.
%   This experiment exists to demonstrate how to use the Latin hypercube
%   sampling (LHS) framework to perform an experiment with an arbitrary model.
%
%   mat_file = EXAMPLE_LHS_EXPERIMENT();
%
%   MAT_FILE is the name of the .mat file that contains the results of the LHS
%   experiment.

% Copyright 2010, 2012 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [mat_file] = example_LHS_experiment()
    % load the model
    model = example_SIR_model();

    % define the parameter distributions
    param_dists = model.default_dists('severe');
    % NOTE: param_dists CAN NOT alter dependent distributions (I0 and beta)

    % choose LHS sample size
    samples = 10
    

    % define the control parameters (legacy, use 1-dimension 'dummy' witha
    % single call)
    c_param1 = struct(...
        'name', 'dummy', ...
        'description', 'Dummy variable', ...
        'values', 1);
    
    % run the LHS simulations and save the results
    [mat_file] = lhs(model, param_dists, samples, c_param1);
end
