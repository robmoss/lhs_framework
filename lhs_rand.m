%LHS_RAND   Select a value at random from some interval of a distribution.
%   For each simulation in a LHS experiment, the value of each model parameter
%   is chosen at random from a specific interval of the probability
%   distribution associated with that parameter.
%
%   value = LHS_RAND(ss, dist, sample);
%
%   SS is the current snapshot of the LHS experiment.
%
%   DIST is the probability distribution for the model parameter.
%
%   SAMPLE is the interval from which the random value must be chosen.
%
%   VALUE is the random value chosen for the model parameter.
%
%   Example:
%      % Choose a random value from the third interval of some distribution
%      >> value = lhs_rand(ss, some_dist, 3);
%
%See also lhs, lhs_resume

% Copyright 2010, 2013 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function [value] = lhs_rand(ss, dist, sample, stream_name)
    % select a random value from a subspace of the unit interval
    val = (sample - rand(ss.(stream_name), size(sample))) ./ ss.samples;
    % return the corresponding value from the probability distribution
    value = dist(val);
end
