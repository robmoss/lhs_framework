% plot_dists.m
%
% Plots the parameter distributions for Latin hypercube sampling.
%

% Copyright 2010 James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function plot_dists(dists)

    function write_dist_data(fid, pname, safe_name, pdist)
        info = pdist();
        dist_name = info.name;
        params = rmfield(info, 'name');
        param_names = fieldnames(params);

        begin_tabular_line = strcat('\\begin{tabular}{', ...
            repmat('c', 1, length(param_names)), '}\n')';
        str_names = param_names{1};
        str_values = strcat('$', num2str(params.(param_names{1})), '$');
        for i = 2:length(param_names)
            str_names = [str_names, ' & ', strrep(param_names{i}, '_', '-')];
            value = num2str(params.(param_names{i}));
            str_values = [str_values, ' & $', value, '$'];
        end
        str_names = strcat(str_names, ' \\\\\n');
        str_values = strcat(str_values, ' \\\\\n');
        dist_name = [upper(dist_name(1)) dist_name(2:end)];

        fprintf(fid, '\\begin{figure}[!h]\n');
        fprintf(fid, '\\begin{center}\n');
        fprintf(fid, '\\includegraphics[width=\\textwidth]{param_%s}\n', ...
            pname);
        fprintf(fid, begin_tabular_line);
        fprintf(fid, '\\toprule\n');
        fprintf(fid, str_names);
        fprintf(fid, '\\midrule\n');
        fprintf(fid, str_values);
        fprintf(fid, '\\bottomrule\n');
        fprintf(fid, '\\end{tabular}\n');
        fprintf(fid, '\\caption{%s probability distribution for %s}\n', ...
            dist_name, safe_name);
        fprintf(fid, '\\end{center}\n');
        fprintf(fid, '\\end{figure}\n');
        fprintf(fid, '\n\\clearpage\n\n');
    end

    fid = fopen('doc/dist_graphs.tex', 'w');

    names = fieldnames(dists);
    h = figure;
    for param = 1:length(names)
        % Retrieve the parameter name and distribution
        pname = names{param};
        safe_name = strrep(pname, '_', '\_');
        
        pdist = dists.(pname);
        if (~iscell(pdist))
            info = pdist();
            if ~strcmp(info.name,'constant')
                % Plot the CDF of the distribution
                plot_dist(pdist);
                % Save the CDF to an external file
                print(gcf, '-depsc2', strcat('doc/images/param_', pname, '.eps'));
                % Output LaTeX code to display the CDF
                write_dist_data(fid, pname, safe_name, pdist);
            end
        else
            % Loop over all elements of the cell (handle mxn)...
            for m = 1:size(pdist,1)
                for n = 1:size(pdist,2)
                    info = pdist{m,n}();
                    if ~strcmp(info.name,'constant')
                        plot_dist(pdist{m,n})
                        print(gcf, '-depsc2', strcat('doc/images/param_', ...
                            pname,'-', num2str(m),'-', num2str(n), '.eps'));
                        write_dist_data(fid, strcat(pname,'-',num2str(m),'-',num2str(n)), ...
                            strcat(safe_name,'-',num2str(m),'-',num2str(n)), pdist{m,n})
                    end
                end
            end
        end
    end
    close(h);
    fclose(fid);
end
