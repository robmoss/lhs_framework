%%GET_STAT    Extract a statistic vector from an LHS simulation.
%
%    data = most_recent('MyScenario');
%    values = get_stat(data, 'AttackRate');
%

% Copyright 2016 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [values] = get_stat(data, stat_name)
    n = length(data.lhs_data.unsampled_params.pi);
    layout = getfield(data.lhs_data.stat_layout, stat_name);
    want_size = layout(2:end);
    first_coord = ones(size(want_size));
    ix0 = stat_index(data.lhs_data.stat_layout, stat_name, first_coord);
    ix1 = stat_index(data.lhs_data.stat_layout, stat_name, want_size);
    values = squeeze(data.lhs_data.stats(1, :, ix0:ix1));
    if (~ all(size(values) == want_size))
        values = reshape(values, [data.lhs_data.samples want_size]);
    end
end
