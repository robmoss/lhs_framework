%GLM_DEVIANCE   Calculate the deviance between a GLM and a LHS experiment.
%   Given a GLM that has been fitted to an entire LHS experiment or some
%   partition of that experiment, GLM_DEVIANCE calculates the deviance between
%   the GLM and the results of the LHS experiment across any number of
%   partitions.
%
%   [dev dev_low dev_high] = GLM_DEVIANCE(lhs_data, partitions, names, stat, b,
%   'PropertyName', PropertValue);
%
%   LHS_DATA is the LHS experiment from which the GLM was derived.
%
%   PARTITIONS is a set of partitions for the LHS experiment.
%
%   NAMES is a cell array containing the names of the parameters that have been
%   chosen as observations for the GLM.
%
%   STAT is the name or index of an output statistic for the LHS experiment.
%
%   B is the vector of coefficient estimates for the GLM.
%
%   'stats' provides details about the GLM (see GLMFIT for more information).
%   This optional argument must be specified in order for GLM_DEVIANCE to
%   return DEV_LOW and DEV_HIGH.
%
%   'link' defines the link function that was used to fit the GLM; the default
%   value is 'identity' (see GLMFIT for more information).
%
%   'fn' is the transformation to apply to the output statistic before
%   comparing the output statistic to values returned by the GLM; the default
%   value is to perform no transformation.
%
%   DEV is the sum of the squared differences between the GLM predictions and
%   the values of the output statistic in LHS_DATA.
%
%   DEV_LOW and DEV_HIGH are the sum of the squared differences between the 95%
%   confidence bounds for the GLM predictions and the values of the output
%   statistic in LHS_DATA.
%
%   Example
%      % Randomly partition the experiment into seven partitions
%      >> parts = partition_randomly(lhs_data, 7);
%      % A binary function of attack rate, 1 means the epidemic was mitigated
%      >> fn = @(x) 0 + (x < 0.1);
%      % Derive a 4-parameter GLM for the attack rate, fitted to partition 5
%      >> [b dev names rho stats] = glm_for_stat(lhs_data, 'Attack Rate', 4,
%            'partition', parts == 5, 'distr', 'binomial', 'link', 'logit',
%            'fn', fn);
%      % Calculate the GLM deviance for each of the 5 partitions
%      >> dev = glm_deviance(lhs_data, parts, names, 'Attack Rate', b, 'link',
%            'logit', 'fn', fn);
%
%See also glmfit, glm_for_stat, glm_roc

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [dev dev_low dev_high] = glm_deviance(lhs_data, partitions, names, stat, b, varargin)
    p = inputParser();
    p.addRequired('lhs_data', @isstruct);
    p.addRequired('partitions', @isnumeric);
    p.addRequired('names', @iscellstr);
    p.addRequired('stat', @ischar);
    p.addRequired('b', @isnumeric);
    p.addParamValue('stats', 0, @isstruct);
    p.addParamValue('link', 'identity', @ischar);
    p.addParamValue('fn', @(x) x);
    p.parse(lhs_data, partitions, names, stat, b, varargin{:});
    
    has_stats = isstruct(p.Results.stats);

    part_count = max(partitions(:));
    dev = zeros(1, part_count);
    if has_stats
       dev_low = zeros(1, part_count);
       dev_high = zeros(1, part_count);
    end

    % calculate the GLM deviance for each partition of the LHS experiment
    for pt = 1:part_count
        part = partitions == pt;
        % collect the observations for each parameter
        obs_mat = partition_parameters(lhs_data, part, names);
        % collect the output statistic
        stat_vec = p.Results.fn( partition_statistic(lhs_data, part, stat) );

        % calculate the GLM deviance for every observation
        % and record the sum of the squared differences
        if has_stats
            [glm_value glm_low glm_high] = ...
                glmval(b, obs_mat, p.Results.link, p.Results.stats);
            % calculate the 95% confidence bounds
            glm_low = glm_value - glm_low;
            glm_high = glm_value + glm_high;
            % calculate the sums of the squared differences
            dev(pt) = sum( (stat_vec - glm_value) .^ 2);
            dev_low(pt) = sum( (stat_vec - glm_low) .^ 2);
            dev_high(pt) = sum( (stat_vec - glm_high) .^ 2);
        else
            glm_value = glmval(b, obs_mat, p.Results.link);
            % calculate the difference in average expected value
            dev(pt) = mean(glm_value) - mean(stat_vec);
        end
    end
end
