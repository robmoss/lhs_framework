function percent = lhs_parfor_progress(tmp_fname,N)
%PARFOR_PROGRESS Progress monitor (progress bar) that works with parfor.
%   PARFOR_PROGRESS works by creating a file called parfor_progress.txt in
%   your working directory, and then keeping track of the parfor loop's
%   progress within that file. This workaround is necessary because parfor
%   workers cannot communicate with one another so there is no simple way
%   to know which iterations have finished and which haven't.
%
%   PARFOR_PROGRESS(N) initializes the progress monitor for a set of N
%   upcoming calculations.
%
%   PARFOR_PROGRESS updates the progress inside your parfor loop and
%   displays an updated progress bar.
%
%   PARFOR_PROGRESS(0) deletes parfor_progress.txt and finalizes progress
%   bar.
%
%   To suppress output from any of these functions, just ask for a return
%   variable from the function calls, like PERCENT = PARFOR_PROGRESS which
%   returns the percentage of completion.
%
%   Example:
%
%      N = 100;
%      parfor_progress(N);
%      parfor i=1:N
%         pause(rand); % Replace with real code
%         parfor_progress;
%      end
%      parfor_progress(0);
%
%   See also PARFOR.

% By Jeremy Scheff - jdscheff@gmail.com - http://www.jeremyscheff.com/

% Patched by James McCaw (jamesm@unimelb.edu.au) to use a temporary
% filename (requires API change, so function call name change)

% Original version Copyright 2011 Jeremy Scheff.
% Modified version Copyright 2012 James McCaw, Patricia Campbell.
% SPDX-License-Identifier: GPL-3.0-or-later

%narginchk(1, 2); commented as function not available in
%2011a. 

if nargin < 2
    N = -1;
end

percent = 0;
w = 50; % Width of progress bar

if N > 0
    f = fopen(tmp_fname, 'w');
    if f<0
        error('Do you have write permissions for %s?', tmp_fname);
    end
    fprintf(f, '%d\n', N); % Save N at the top of tmp_fname
    fclose(f);
    
    if nargout == 0
        disp(['  0%[>', repmat(' ', 1, w), ']']);
    end
elseif N == 0
    delete(tmp_fname);
    percent = 100;
    
    if nargout == 0
        disp([repmat(char(8), 1, (w+9)), char(10), '100%[', repmat('=', 1, w+1), ']']);
    end
else
    if ~exist(tmp_fname, 'file')
        error('Temporary counter file not found. Run LHS_PARFOR_PROGRESS(<temp filename>, N) before LHS_PARFOR_PROGRESS to initialize counter file.');
    end
    
    f = fopen(tmp_fname, 'a');
    fprintf(f, '1\n');
    fclose(f);
    
    f = fopen(tmp_fname, 'r');
    progress = fscanf(f, '%d');
    fclose(f);
    percent = (length(progress)-1)/progress(1)*100;
    
    if nargout == 0
        perc = sprintf('%3.0f%%', percent); % 4 characters wide, percentage
        disp([repmat(char(8), 1, (w+9)), char(10), perc, '[', repmat('=', 1, round(percent*w/100)), '>', repmat(' ', 1, w - round(percent*w/100)), ']']);
    end
end