# Latin Hypercube Sampling (LHS) framework for Matlab

This framework efficiently samples multi-dimensional parameter spaces to
explore the variability in model dynamics in an efficient manner, by using
[Latin hypercube sampling](https://en.wikipedia.org/wiki/Latin_hypercube_sampling).

## Repository Layout

This directory contains the code and documentation for the LHS framework.

+ `lhs_framework`: the LHS framework code.
  + `doc`: documentation and user guide for the LHS .
  + `solvers`: fixed-step ODE solvers.

## License

Copyright (C) 2010-2016, Robert Moss, James McCaw, Mathew Dafilis, Patricia Campbell.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

## Installation

Add the LHS framework (including subdirectories) to the MATLAB search path:

    addpath(genpath(pwd));

## Example Model and Experiment

A toy SIR model is provided as an example of how to use the LHS framework.

The model is defined in `example_SIR_model.m` and an LHS experiment is defined
in `example_LHS_experiment.m`.

## Related publications

This framework has been used in a number of reports to the Australian
Department of Health and the World Health Organization, as well as in the
following publications:

+ [Moss R et al., *medRxiv*
   2020](https://doi.org/10.1101/2020.04.07.20056184)
+ [McCulloch K et al., *Hepatology* 71(4),
   2019](https://doi.org/10.1002/hep.30899)
+ [Moss R et al., *BMC ID* 16(1),
   2016](https://doi.org/10.1186/s12879-016-1866-7)
+ [Campbell PT et al., *Vaccine* 33(43),
   2015](https://doi.org/10.1016/j.vaccine.2015.09.025)
+ [Moss R, McCaw JM, McVernon J, *PLOS ONE* 6(2),
   2011](https://doi.org/10.1371/journal.pone.0014505)
