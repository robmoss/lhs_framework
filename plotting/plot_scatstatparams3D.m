function figures = plot_scatstatparams3D(param1,p1_fn,p1axis,param2,p2_fn,p2axis,stats,stat_fn, varargin)
% plot_scatstatparams3D
% Plots stat vs param1 coloured by param2
% Arguments
% param1, param2 == parameters
% p1_fn,p2_fn,stat_fn == pass '@(x) x' if you just want the param or stat
% p1axis,p2axis == 'lin' or 'log' for appropriate axes scaling
% stats = output statistic
% varargin == expt.lhs_data
% 
% Parameters or output stats can be specified with either name or number.
% Based very heavily on Rob's plot_statistics.m

% Copyright 2012 James McCaw, Mathew Dafilis.
% SPDX-License-Identifier: GPL-3.0-or-later

% Test to see if any experiments in varargin    
    if isempty(varargin) % If none bye-bye
        error('plot_statparams:experiments', 'No experiments given');
    end

    % Statistics manipulation
    
    exp_count = length(varargin);
     first_exp = varargin{1};
     stat_names = first_exp.stat_names;
     % the number of statistics is the size of
     % the final dimension of the stats matrix
     stats_dim = size(first_exp.stats);
     stats_available = stats_dim(end);
% for i = 1:length(stats)
     
     if ischar(stats)
         stats = {stats};
     end
     if iscellstr(stats)
         [~, stat_nums] = ismember(stats, stat_names);
         if any(stat_nums < 1)
             error('plot_statistics:stats', 'Invalid statistics name');
         end
     elseif isnumeric(stats)
         stat_nums = stats;
         if any(stat_nums > stats_available)
            error('plot_statistics:stats', 'Invalid statistics index');
         end
     else
         error('plot_statistics:stats', 'Invalid statistics parameter');
     end
    
     % Parametric manipulation
     
     param_names=fieldnames(first_exp.parameter_samples);
     param_dim=size(param_names);
     param_available=param_dim(end);
     
     if ischar(param1)
         param1={param1};
     end
     
     if iscellstr(param1)
         [~,param_nums] = ismember(param1,param_names);
     end
     if any(param_nums < 1)
             error('plot_paramstats3D:param1', 'Invalid parameter index');
     end
     
     if ischar(param2)
         params={param2};
     end
     
     if iscellstr(param2)
         [~,param_nums] = ismember(param2,param_names);
     end
     if any(param_nums < 1)
         error('plot_paramstats3D:param1', 'Invalid parameter index');
     end
     
     first_exp=varargin{1};
     if ~isfield(first_exp,'stat_layout')
         stat_entries = stat_nums;
         stat_name = first_exp.stat_names{stat_nums};
     else % stats are a structure so stat_nums(i) may refer to multiple entries in stats_dim
         seq_of_entries = structfun(@(x) x(1), first_exp.stat_layout);
         cum_seq_of_entries = [0 ; cumsum(seq_of_entries(1:end-1))];
         stat_entries = (1:seq_of_entries(stat_nums)) + cum_seq_of_entries(stat_nums);
         stat_name = stats;
     end
     
     %stat_nums=stats;

	% Create a new figure
	figures = figure();
    hold on; % Keep plots
    for i = 1:length(varargin);   % Loop over number of experiments
        exp=varargin{i};	% Pick out an experiment
        stat_data = stat_fn(exp.stats(:, :, stat_entries)); % Get the stat data
        param1_data=p1_fn(getfield(exp.parameter_samples, char(param1))); % Get the param data
        param2_data=p2_fn(getfield(exp.parameter_samples, char(param2)));
        scatter(param1_data,stat_data,12,param2_data,'s','filled');
        colorbar;
        xlabel(param1);
        %            ylabel(param2); % Label the graph
        ylabel(exp.stat_names(stat_nums))
        %	    view(135,45)
        if char(p1axis) == 'log'
            set(gca,'XScale','log')
        end
        %	  if char(p2_fn) == 'log'
        %		set(gca,'YScale','log')
        %	end
    end

