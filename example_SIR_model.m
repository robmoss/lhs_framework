%EXAMPLE_SIR_MODEL   An example of a simple SIR model.
%   This model exists to demonstrate how to apply the Latin hypercube sampling
%   (LHS) framework to an arbitrary model.
%
%   model = EXAMPLE_SIR_MODEL();
%
%   MODEL is a record (structure array) that contains the model details.
%
%See also example_LHS_experiment, lhs

% Copyright 2010, 2012 Robert Moss, James McCaw.
% SPDX-License-Identifier: GPL-3.0-or-later
function model = example_SIR_model()
    % collect the various parts of the model into a single structure
    model = struct( ....
        'params', @params, 'unsaved_params', @unsaved_params, ...
        'state', @state, 'statistics', @stats, ...
        'default_dists', @def_dists, 'dependent_dists', @dep_dists, ...
        'events', @events, 'equations', @rhs ...
        );
end

%PARAMS   Return the default values of the model parameters.
%
%   params = PARAMS();
%
%   PARAMS is a record (structure array) containing the default parameter
%   values.
function params = params()
    % NOTE: time_start, time_end and time_entries are always necessary
    %       solve_discretely defaults to 0 if it is not specified
    params = struct( ...
        'dummy', 1, ... % dummy variable to act as a "control" parameter (see example_LHS_experiment.m)
        'N', 1e4, ... % a population of 10,000
        'Z', 0.1, ... % 10% are immune
        'I0', 5, ... % 10 initial cases in the population
        'R0', 1.5, ... % an R0 of 1.5
        'gamma', 365 / 1, ... % an infectious phase of 1 day
        'beta', 547.5, ... % beta = R0 * gamma
        'solve_discretely', 1, ... % always solve discretely
        'time_start', 0, ... % start simulations at time t=0
        'time_end', 42 / 365, ... % end simulations at 6 weeks
        'time_entries', 42 ... % record the model state once per day
        );
end

%UNSAVED_PARAMS List parameters that wont be stored on completion
%
%   unsaved_params = UNSAVED_PARAMS();
%
%   UNSAVED_PARAMS is a cell array of parameter names (strings).
function unsaved_params = unsaved_params()
    unsaved_params = {'dummy','solve_discretely'};
end

%STATE   Return the initial model state.
%   Return the initial state of the model for a given set of parameter values.
%
%   state = STATE(params);
%
%   PARAMS is a record (structure array) that defines the value of each model
%   parameter.
%
%   STATE is a vector that specifies the initial value of each state variable
%   in the model.
function state = state(params)
    R = params.Z * params.N;
    I = params.I0;
    S = params.N - (sum(I(:)) + R);
    state = struct('S', S, 'I', I, 'R', R);
end

%STATS   Record output statistics for a single model simulation.
%
%   output = STATS(time, params, state, events);
%
%   TIME is the vector of times at which the model state was recorded.
%
%   PARAMS is a record (structure array) that defines the value of each model
%   parameter.
%
%   STATE is a matrix that contains the evolution of each state variable over
%   the time period specified by TIME.
%
%   EVENTS is a private copy of the global variable events, which records any
%   events of interest that occurred during the simulation.
%
%   OUTPUT is a vector of output statistics for the given model simulation.
function output = stats(time, params, state, events)
    if nargin == 2
        % Return the names of the statistics being recorded
        output = struct('AttackRate', 1, 'AR_50pcnt', 1, ...
            'TotalInfections', 1, 'I_max', 1);
        return;
    elseif nargin ~= 4
        fprintf(1, 'ERROR: invalid # of parameters to statistics\n');
        return;
    end
    
    % Extract the time-series for each state variable
    S = state.S;
    I = state.I;
    R = state.R;

    % Calculate the desired statistics
    init_R = params.Z * params.N;
    total_infections = R(end) - init_R;
    attack_rate = total_infections / params.N;
    attack_rate_50 = 365 * ...
        time(find( (R - init_R) / params.N >= 0.5 * attack_rate, 1 ));

    % Record all of the statistics
    output = struct( ...
        'AttackRate', attack_rate, 'AR_50pcnt', attack_rate_50, ...
            'TotalInfections', total_infections, 'I_max', max(I));
end

%DEF_DISTS   Returns probability distributions for independent parameters.
%   Returns default probability distributions for all model parameters that are
%   allowed to vary from their default values.
%
%   dists = DEF_DISTS(scenario);
%
%   SCENARIO is an (optional) string for specifying that scenario-specific
%   probability distributions should be returned. This parameter should not
%   be specified, or set to the value 'severe'.
%
%   DISTS is a record (structure array) that associates a probability
%   distribution with each independent model parameter.
function dists = def_dists(scenario)
    % distributions are only needed for parameters that should be varied
    % parameters that should only take their default value can be ignored
    dists = struct( ...
        'R0', dist_beta(1.5, 0.1, 1.1, 1.9), ... % R0 varies from 1.1 to 1.9
        'Z', dist_flat(0.05, 0.15) ... % 5% to 15% are initially immune
        );

    if nargin > 0
        if strcmp(scenario, 'severe')
            % make R0 higher, to produce a more severe epidemic
            dists.R0 = dist_beta(1.8, 0.1, 1.4, 2.2);
        else
            % warn that an unrecognised scenario was specified
            warning('example_SIR_model:scenario', ...
                'Unrecognised scenario "%s"', scenario);
        end
    end
end

%DEP_DISTS   Returns probability distributions for dependent model parameters.
%   The antiviral SEIR model contains several dependent parameters, whose
%   values are derived from (or influenced by) independent parameters.
%
%   dists = DEP_DISTS(params);
%
%   PARAMS is a record (structure array) that defines the value of each
%   independent model parameter.
%
%   DISTS is a record (structure array) that associates a probability
%   distribution with each dependent model parameter.
function dists = dep_dists(params)
    % initial cases are (on average) 0.9% of the susceptible population
    % therefore, the number of cases depends on both N and Z
    I0_mean = params.N * (1 - params.Z) * 0.0045;
    I0_var = 0.15 * I0_mean;
    I0_min = 0.5 * I0_mean;
    I0_max = 1.5 * I0_mean;
    distI = dist_beta(I0_mean, I0_var, I0_min, I0_max);

    % return the probability distributions
    dists = struct( ...
        'I0', distI, ...
        'beta', dist_const(params.R0 * params.gamma) ...
        );
end

%EVENTS   Initialises the events record for a simulation.
%   During each model simulation, various events and conditions can arise. This
%   function returns an initialised record (structure array) for recording any
%   such events that occur during a single simulation.
%
%   events = EVENTS(initial_state, params);
%
%   INITIAL_STATE is a vector that specifies the initial value of each state
%   variable in the model.
%
%   PARAMS is a record (structure array) that defines the value of each
%   independent and dependent model parameter.
%
%   EVENTS is an initialised record (structure array) for recording any events
%   of interest that occur during a single simulation.
function events = events(initial_state, params)
    events = struct( ...
        'largeCaseLoad', 0, ... % set to 1 when I exceeds 100
        'timeDoubledR', -1 ... % set when R becomes twice as big as at t = 0
        );
end

%RHS   Calculates the rate of change for each state variable in the model.
%   This function encapsulates the system of equations that describe the model.
%   That is, y' = RHS(t, y, params);
%
%   rates = RHS(time, state, params);
%
%   TIME is the current time-point in the simulation.
%
%   STATE is a vector that specifies the current value of each state variable
%   in the model.
%
%   PARAMS is a record (structure array) that defines the value of each model
%   parameter.
%
%   RATES is a vector that specifies the rate of change for each state variable
%   in the model.
function rates = rhs(time, state, params)
    global events;

    % extract the individual states from the current state vector
    S = state.S;
    I = sum(state.I);
    R = state.R;

    % check whether any events of interest have occurred
    if I > 100
        events.largeCaseLoad = 1;
    end
    if events.timeDoubledR == -1 && R >= 2 * params.N * params.Z
        events.timeDoubledR = time;
    end

    % calculate the flows between the states
    S_to_I = params.beta * I * S / params.N;
    I_to_R = params.gamma * I;

    % use stochastic flows if a discrete solver is being used and the
    % deterministic flow is less than 10 (persons per year)
    if params.solve_discretely && S_to_I < 10
        stoch_vals = events.stochStream.rand([floor(S) 1]);
        pr = params.beta * I / params.N;
        % NOTE: if beta * I > N, the stochastic rate is clipped at S
        % this is not an issue, since stochastic flows are only used
        % when the corresponding deterministic flows are very small
        S_to_I = sum(stoch_vals < pr);
    end
    if params.solve_discretely && I_to_R < 10
        stoch_vals = events.stochStream.rand([floor(I) 1]);
        pr = params.gamma;
        I_to_R = sum(stoch_vals < pr);
    end

    % calculate the net flow rate into each state
    dS = - S_to_I;
    dI = S_to_I - I_to_R;
    dR = I_to_R;

    % return the flow rates (as a column vector, not a row vector)
    rates = struct('S', dS, 'I', dI * 0.5, 'R', dR);
end
