%REORDER_PLOT   Reorders the stack of plots on a set of axes.
%
%   REORDER_PLOT(fn, axes);
%
%   FN is a function that accepts a vector of children handles (top-most to
%   bottom-most) and returns a reordered vector of children handles.
%
%   AXES specifies the axes whose plots will be reorganised. This argument is
%   optional and defaults to the value returned by GCA().
%
%   Example:
%      % Reverse the order of the plots
%      >> reorder_plot(@flipud);

% Copyright 2010 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function reorder_plot(fn, axes)
    if nargin < 2
        axes = gca();
    end
    lines = findobj(axes, 'Type', 'line');
    parent = get(lines(1), 'Parent');
    children = get(parent, 'Children');
    new_children = fn(children);
    set(parent, 'Children', new_children);
end
